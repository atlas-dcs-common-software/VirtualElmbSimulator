/*
 * UaTraceSink.cpp
 *
 *  Created on: May 26, 2015
 *      Author: pnikiel
 */

#include <UaTraceSink.h>
#include <uatrace.h>

#include <iostream>


bool UaTraceSink::initialize()
{
	std::cout << "UaTraceSink::initialize" << std::endl;
	return true;
}


void UaTraceSink::logMessage(const std::string& msg)
{
	UaTrace::tError("%s", msg.c_str());
}
