/*
 * componentAttributes.cpp
 *
 *  Created on: Mar 5, 2015
 *      Author: bfarnham
 */

#include "ComponentAttributes.h"

ComponentAttributes::ComponentAttributes(const uint32_t& id, const std::string& name, const Log::LOG_LEVEL& level/*=INF*/)
:m_id(id), m_name(name), m_level(level)
{}

ComponentAttributes::ComponentAttributes(const ComponentAttributes& obj)
:m_id(obj.m_id), m_name(obj.m_name), m_level(obj.m_level)
{}

ComponentAttributes& ComponentAttributes::operator=(const ComponentAttributes& obj)
{
    if(&obj == this) return *this;

    m_id = obj.m_id;
    m_name = obj.m_name;
    m_level = obj.m_level;

    return *this;
}

uint32_t ComponentAttributes::getId() const
{
    return m_id;
}

std::string ComponentAttributes::getName() const
{
    return m_name;
}

Log::LOG_LEVEL ComponentAttributes::getLevel() const
{
    return m_level;
}

void ComponentAttributes::setLevel(const Log::LOG_LEVEL& level)
{
    m_level = level;
}
