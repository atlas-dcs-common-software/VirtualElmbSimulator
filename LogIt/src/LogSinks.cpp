/*
 * LogSinks.cpp
 *
 *  Created on: May 15, 2015
 *      Author: bfarnham
 */
#include "LogSinks.h"

using std::vector;

LogSinks* LogSinks::g_sLogSinksInstance;

void LogSinks::addSink(LogSinkInterface* sink)
{
    m_sinks.push_back(sink);
}

void LogSinks::logMessage(const std::string& logMsg)
{
    for(vector<LogSinkInterface*>::iterator it = m_sinks.begin(); it!= m_sinks.end(); ++it)
    {
        (*it)->logMessage(logMsg);
    }
}
