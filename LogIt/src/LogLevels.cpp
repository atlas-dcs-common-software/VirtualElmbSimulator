#include "LogLevels.h"
#include <boost/algorithm/string.hpp>
#include <LogIt.h>

std::string Log::logLevelToString(const LOG_LEVEL& level)
{
    switch(level)
    {
        case(TRC): return "TRC";
        case(DBG): return "DBG";
        case(INF): return "INF";
        case(WRN): return "WRN";
        case(ERR): return "ERR";
        default  : return "UNKNOWN";
    }
}

bool Log::logLevelFromString(const std::string &s, LOG_LEVEL &out)
{
	const std::string levelString = boost::to_upper_copy(s);

	if (levelString=="ERR" || levelString=="ERROR" )	out=Log::ERR;
	else if	(levelString=="WRN" || levelString=="WARN" || levelString=="WARNING" ) out=Log::WRN;
	else if	(levelString=="INF" || levelString=="INFO" || levelString=="INFORMATION") out = Log::INF;
	else if	(levelString=="DBG" || levelString=="DEBUG")	out = Log::DBG;
	else if	(levelString=="TRC" || levelString=="TRACE") out = Log::TRC;
    else
    {
    	LOG(Log::WRN) << "failed to parse log level string ["<<s<<"] to log level";
    	return false;
    }
    return true;
}
