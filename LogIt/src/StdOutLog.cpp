/*
 * StdOutLog.cpp
 *
 *  Created on: May 19, 2015
 *      Author: pnikiel
 */

#include <StdOutLog.h>
#include <iostream>



bool StdOutLog::initialize()
{
	std::cout << "StdOutLog::initialize" << std::endl;
	return true;
}

void StdOutLog::logMessage(const std::string& msg)
{
	std::cout << msg << std::endl;
}
