/*
 * LogSinkInterface.h
 *
 *  Created on: May 15, 2015
 *      Author: bfarnham
 */

#ifndef SRC_INCLUDE_LOGSINKINTERFACE_H_
#define SRC_INCLUDE_LOGSINKINTERFACE_H_

#include <string>

class LogSinkInterface
{
public:
    LogSinkInterface(){};
    virtual ~LogSinkInterface(){};

    /**
     * Expected to be called once on initialization.
     * returns true if initialization successful, otherwise false
     */
    virtual bool initialize() = 0;

    /**
     * Expected to be called often, sends msg to logger backend.
     */
    virtual void logMessage(const std::string& msg) = 0;
};



#endif /* SRC_INCLUDE_LOGSINKINTERFACE_H_ */
