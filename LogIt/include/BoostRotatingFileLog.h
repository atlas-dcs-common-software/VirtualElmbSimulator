/*
 * LogSinks.h
 *
 *  Created on: Mar 9, 2015
 *      Author: bfarnham
 */

#ifndef BOOST_ROTATING_FILE_LOG_H_
#define BOOST_ROTATING_FILE_LOG_H_

#include <string>
#include "LogSinkInterface.h"

class BoostRotatingFileLog : public LogSinkInterface
{
public:
    virtual bool initialize();
    virtual void logMessage(const std::string& msg);

private:
    void addFileSink();
};

#endif /* BOOST_ROTATING_FILE_LOG_H_ */
