/*
 * LogRecord.h
 *
 *  Created on: Mar 9, 2015
 *      Author: bfarnham
 */

#ifndef LOGRECORD_H_
#define LOGRECORD_H_

#include <stdint.h>
#include <sstream>
#include "LogLevels.h"

class LogRecord
{
public:
    LogRecord(const std::string& file, const int& line, const Log::LOG_LEVEL& level);
    LogRecord(const std::string& file, const int& line, const Log::LOG_LEVEL& level, const uint32_t& componentId);
    virtual ~LogRecord();

    std::ostream& getStream();

private:
    static const std::string stripDirectory(const std::string& file);
    std::ostringstream& initializeStream(const std::string& file, const int& line, const Log::LOG_LEVEL& level);
    std::ostringstream m_stream;
};



#endif /* LOGRECORD_H_ */
