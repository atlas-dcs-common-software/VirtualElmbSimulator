/*
 * StdOutLog.h
 *
 *  Created on: May 19, 2015
 *      Author: pnikiel
 */

#ifndef LOGIT_INCLUDE_STDOUTLOG_H_
#define LOGIT_INCLUDE_STDOUTLOG_H_

#include <LogSinkInterface.h>

class StdOutLog: public LogSinkInterface
{
public:

    virtual bool initialize();
    virtual void logMessage(const std::string& msg);
};



#endif /* LOGIT_INCLUDE_STDOUTLOG_H_ */
