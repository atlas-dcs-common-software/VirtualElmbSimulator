/*
 * LogLevels.h
 *
 *  Created on: Mar 5, 2015
 *      Author: bfarnham
 */

#ifndef LOGLEVELS_H_
#define LOGLEVELS_H_

#include <string>

namespace Log
{
    enum LOG_LEVEL
    {
        TRC = 0,
        DBG,
        INF,
        WRN,
        ERR
    };

    std::string logLevelToString(const LOG_LEVEL& level);
    bool logLevelFromString(const std::string &s, LOG_LEVEL &out);
}

#endif /* LOGLEVELS_H_ */
