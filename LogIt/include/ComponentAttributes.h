/*
 * componentAttributes.h
 *
 *  Created on: Mar 5, 2015
 *      Author: bfarnham
 */

#ifndef COMPONENTATTRIBUTES_H_
#define COMPONENTATTRIBUTES_H_

#include <string>
#include <stdint.h>
#include "LogLevels.h"

class ComponentAttributes
{
public:
	ComponentAttributes(const uint32_t& id, const std::string& name, const Log::LOG_LEVEL& level = Log::INF);
    ComponentAttributes(const ComponentAttributes& obj);
    ComponentAttributes& operator=(const ComponentAttributes& obj);

    uint32_t getId() const;
    std::string getName() const;

    Log::LOG_LEVEL getLevel() const;
    void setLevel(const Log::LOG_LEVEL& level);

private:
    uint32_t m_id;
    std::string m_name;
    Log::LOG_LEVEL m_level;
};

#endif /* COMPONENTATTRIBUTES_H_ */
