/*
 * LogSinks.h
 *
 *  Created on: May 15, 2015
 *      Author: bfarnham
 */

#ifndef SRC_INCLUDE_LOGSINKS_H_
#define SRC_INCLUDE_LOGSINKS_H_

#include <string>
#include <vector>
#include "LogSinkInterface.h"

class LogSinks
{
public:
    static LogSinks* g_sLogSinksInstance;

    void addSink(LogSinkInterface* sink);
    void logMessage(const std::string& logMsg);

private:
    std::vector<LogSinkInterface*> m_sinks;
};

#endif /* SRC_INCLUDE_LOGSINKS_H_ */
