/*
 * UaTraceSink.h
 *
 *  Created on: May 26, 2015
 *      Author: pnikiel
 */

#ifndef LOGIT_INCLUDE_UATRACESINK_H_
#define LOGIT_INCLUDE_UATRACESINK_H_


#include <string>
#include <LogSinkInterface.h>

class UaTraceSink: public LogSinkInterface
{
public:

    virtual bool initialize();

    virtual void logMessage(const std::string& msg);
};


#endif /* LOGIT_INCLUDE_UATRACESINK_H_ */
