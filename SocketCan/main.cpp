#include "SimulatedCanScan.h"
#include "CUtils.h"
#include "CoreSimulator.hxx"
#include <unistd.h>
#include <iostream>


#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <net/if.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/error.h>
#include <libsocketcan.h>
#include <errno.h>

#include <boost/program_options.hpp>
#include <boost/thread.hpp>

#include <LogIt.h>

using namespace std;
using namespace boost::program_options;

class CCanMessageSender
{
public:
	CCanMessageSender (int sock):
		mSock(sock) {}
	void operator()(const canMessage& msg)
	{
		struct can_frame frame;
		frame.can_id = msg.c_id;
		frame.can_dlc = msg.c_dlc;
		memcpy (frame.data, msg.c_data, sizeof (frame.data));
		while(true)
		{
			int numb = write (mSock, &frame, sizeof (struct can_frame));
			if (numb>=0)
				return;
			else
			{
				perror ("write");
				if (ENOBUFS == errno)
				{
					usleep (50000);
					cout << "ENOBUFS(no buffer space) detected, will retry in  a while (sock="<<mSock<<")" << endl;
				}
				else
					return;
			}
		}
//		else
//			cout << "write succesful" << endl;
	}
	int mSock;
};

void reader (int sock, CSimulatedCanScan *canscan)
{
	cout << "started reader thread" << endl;
	while (true)
	{
		struct can_frame sockmsg;
		int nbytes = read (sock, &sockmsg, sizeof (sockmsg));
		if (nbytes < 0)
		{
			perror ("while read()");
			usleep (100000);
			continue;
		}
		if (nbytes > 0)
		{
//			cout << "read " << nbytes << "bytes " << endl;
			unsigned int can_id = sockmsg.can_id;
			if (can_id & CAN_RTR_FLAG)
			{
				// retranslate CAN_RTR_FLAG which is stored on 32 bits to SIMULATED_RTR_FLAG which is stored on 16 bits
				// because sendMessage interface accepts only 16 bits for can id (short int)
				can_id &= ~CAN_RTR_FLAG;
				can_id |= SIMULATED_RTR_FLAG;
			}
			canscan->sendMessage (can_id, sockmsg.can_dlc, sockmsg.data);

		}
		/* attempt read */
		//sleep (1);
	}

}


int prepareCanPort (const char* portName)
{
	struct sockaddr_can addr;
	struct ifreq ifr;
	int		numPar,err;
	unsigned int br;

//	err = can_do_stop(portName);
//	if (err < 0) {
//		perror("Cannot stop channel");
//		return err;
//	}
//
//	err = can_set_bitrate(portName,125000);
//	if (err < 0) {
//		perror("Cannot set bit rate");
//		return err;
//	}
//	err = can_do_start(portName);
//	if (err < 0) {
//		perror("Cannot start channel");
//		return err;
//	}

	int sock = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	if (sock < 0)   {
       // fill out initialisation struct
		perror("Cannot open the socket");
		return -1;
	}
	memset(&ifr.ifr_name, 0, sizeof(ifr.ifr_name));
	strncpy(ifr.ifr_name, portName, sizeof(portName));

	if (ioctl(sock, SIOCGIFINDEX, &ifr) < 0) {
		perror("SIOCGIFINDEX");
		return -1;
	}

	can_err_mask_t err_mask = 0x1ff;

	setsockopt(sock, SOL_CAN_RAW, CAN_RAW_ERR_FILTER,
	              &err_mask, sizeof(err_mask));


	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("bind");
		return -1;
	}
	return sock;
}

int main (int argc, char* argv[])
{
	Log::initializeLogging(Log::TRC);
	options_description desc ("Allowed options");
	int portshift;
	bool all_interfaces;
	string config_fn;
	desc.add_options()
			("help", "produce help")
			("all", bool_switch(&all_interfaces) )
			("bus_shift", value<int>(&portshift)->default_value(0))
			("can_bus_interface",value< vector<string> >(), "list of interfaces may also appear at the end of the command line")
			("config_fn", value<string>(&config_fn)->default_value("sim_config.xml"))
			;
	positional_options_description p;
	p.add("can_bus_interface", -1);
	variables_map vm;
	store(command_line_parser(argc,argv).options(desc).positional(p).run(),vm);
	notify(vm);
	if (vm.count("help"))
	{
		cout << desc << endl;
		return 1;
	}
	if (vm.count("can_bus_interface") < 1 && !all_interfaces)
	{
		cout << "No CAN interfaces specified! Please specify at least one ... or use --all" << endl;
		return 1;
	}


	if (portshift > 0)
	{
		SimulatedCan::CUtils::portShift = portshift;
		cout << "**** WARNING: using portShift=" << portshift << endl;
	}
	std::vector<std::string> interfaces;
	if (!all_interfaces)
	{
		interfaces = vm["can_bus_interface"].as< vector<string> >();
	}
	else
	{
		::std::unique_ptr< ::Simulation > m_XmlConfigSimulation;
		try
		{
			m_XmlConfigSimulation = simulation(config_fn);
		}
		catch (const xml_schema::exception& e)
		{
			cerr << e << endl;
			return -1;
		}
		::Bus const *bus = 0;
		for (::Simulation::bus_const_iterator it=m_XmlConfigSimulation->bus().begin(); it!=m_XmlConfigSimulation->bus().end(); it++)
		{
			interfaces.push_back((*it).portName());
		}


	}

	for (int i=0; i<interfaces.size(); i++)
	{
		string name = interfaces[i];
		string mappedName;
		SimulatedCan::CUtils::remapCanPort(name,mappedName);
		cout << "Trying to open: (unmapped name)=" << name << " (remapped name)=" << mappedName << endl;
		int sock = prepareCanPort(mappedName.c_str());

		if (sock<0)
		{
			cout << "Init fail " << endl;
			return -1;
		}
		CCanMessageSender* msgSender = new CCanMessageSender (sock);
		CSimulatedCanScan* canscan = new CSimulatedCanScan();
		canscan->canMessageCame.connect (*msgSender);
		canscan->createBUS (name.c_str(), "params");

		boost::thread *t = new boost::thread(reader, sock, canscan);

	}

	cout << "entering main loop ... " << endl;
	while (1)
	{
		sleep (1);
	}


}
