# This creates standaloen executable

set(Boost_NO_BOOST_CMAKE ON) # workaround for boost-1.7.0 cmake config modules: disabling search for boost-cmake to use FindBoost instead
find_package(Boost REQUIRED program_options thread system)
if(NOT Boost_FOUND)
    message(FATAL_ERROR "Failed to find boost installation")
else()
    message(STATUS "Found system boost, version [${Boost_VERSION}], include dir [${Boost_INCLUDE_DIRS}] library dir [${Boost_LIBRARY_DIRS}], libs [${Boost_LIBRARIES}]")
endif()

link_directories (
	${VirtualElmbSimulator_BINARY_DIR}/CoreSimulator
	${VirtualElmbSimulator_BINARY_DIR}/LogIt
	${Boost_LIBRARY_DIRS}
	)
	
include_directories (
	${VirtualElmbSimulator_SOURCE_DIR}/CoreSimulator
	include_directories( ${Boost_INCLUDE_DIRS} )
	)
add_executable (SocketCan main.cpp $<TARGET_OBJECTS:LogIt>)
target_link_libraries (
	SocketCan
	CoreSimulator 
	${Boost_LIBRARIES}
	 -lpthread -lPocoNet -lPocoUtil -lPocoFoundation /usr/local/lib/libsocketcan.a -lxerces-c)
