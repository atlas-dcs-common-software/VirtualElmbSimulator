Version 1.3.6
-------------
[OPCUA-771] Add memory for AI settings

Version 1.3.5
-------------
[OPCUA-660] Add bit-based DO control to ELMB simulator

Version 1.3.4
-------------
[OPCUA-605] Add emulation of no-power / broken-bus to ELMB Simulator

Version 1.3.3
-------------
- fixed format for EMERGENCY errors; now they errorReg is there (though unlikely it would be often used)
- added features necessary to testing of unified Digital IO (i.e. that any port can be either input/output or both)

Version 1.3.2
-------------
- added new command: get_digital_input

Version 1.3.1
-------------
- fixed code for test9999 set/get and TPDO4/RPDO4 handling

Version 1.3.0
-------------
- new minor version because hardware component API has changed due to Slava's request
- a fix with delay time configuration for SDO objects

Version 1.2.3
-------------
- lots of improvements with asterisk commands (i.e. OPCUA-542)
- emergency error can now be sent with optional bytes of error, i.e. only error code, or with 1, 2, or 3 additional bytes

Version 1.2.2
-------------
-- no factual changes, SVN tag problem with 1.2.1

Version 1.2.1 (16-Nov-2015)
---------------------------
- set_conversion_flag supports asterisks in elmb ids and channel numbers

Version 1.2.0 (30-Oct-2015)
--------------------
- Big rework of the simulator, simplifications and cleanup
--- requires c++11 now !!!!

Version 1.1.7 (21-Jan-2015)
---------------------------
- Implemented control of channel conversion flags

Version 1.1.6 (20-Jan-2015)
---------------------------
- Support for special Object Dictionary index=9999 for testing various
  datatypes handling

Version 1.1.5 (5-Nov-2014)
--------------------------
- Got rid of disabling LOOPBACK mode in CAN initialization code (this killed
  VCAN interfaces)
 

Version 1.1.4 (17-Apr-2014)
---------------------------
- Improved documentation
- Fixed bugs that caused Valgrind to point out errors.

Version 1.1.3 (7-Mar-2014)
--------------------------
- Ability to simulate disconnected ELMBs, like the CAN was not connected
  this is achievable using TextCommand set_can_connectivity
  e.g.
  set_can_connectivity can0 <nodeid> <connectivity>

Version 1.1.2 (20-Mar-2014)
---------------------------
- Ability to send emergency objects from a simulated ELMB


Version 1.0.9 (22-Jan-2014)
- Without debug flags activated, much less crap is printed to a screen.

Version 1.0.8 (21-Jan-2014)
---------------------------
- CSimulatedCanScan::createBUS fix from version 1.0.7 was not perfect -
  improving this.
 
Version 1.0.7 (17-Jan-2014)
---------------------------
- CSimulatedCanScan::createBUS will now accept busname both in short way
  (can0) as in long way (sock:can0), so the Simulator should properly work
  with the server 2.0

Version 1.0.6 (16-Jan-2014)
---------------------------
- Object 6200 from ELMB Object Directory added. (that was an important feature
  to have for a number of Server tests).
- Fix for a RPM install script which would remove ld.so.conf.d information
  when packaged would be about to be upgraded

Version 1.0.5 (19-Dec-2013)
---------------------------
- Every write SDO of expedited type (that is: different than segmented) will have now confirmation frame sent, 
  however the information sent will not yet be properly stored (planned for next version)

Version 1.0.4 (18-Dec-2013)
---------------------------
- Minimal allowable number of nodes on a bus is now 0
- A new feature: randomizer: let's simulate bad (random) frames on a bus

Version 1.0.3 (15-Dec-2013)
---------------------------
- A feature to log every CAN frame received by the ELMBs and then dump it to file (dump is activate through Text Commands)
- Delay Generator: new feature, now might by used by segmented SDO. It enables to use either constant or randomzied delayds.

Version 1.0.2 (11-Dec-2013)
---------------------------
- When SocketCan spots 'No Buffer Space' message, a transfer will be resumed instead of being lost
- Segmented SDO Delay (That is a time between receiving segmented sdo segment and sending response) is now configurable from XML config file
- Data received in segmented SDO may now be dumped easily to a file

Version 1.0.1 (9-Dec-2013)
--------------------------
- New Text command: sendbootup <bus> <node> - allows to provoke  bootup message
- Fixed a problem in TPDO3 in which only 16-bit value was sent instead of 32-bit (that corrupted negative measurements)

Version 1.0.0 (7-Dec-2013)
--------------------------
- Text Commands support ( 3 supported)
- XML Config only
- Multiple cleanups


Version 0.9.3 (31-Oct-2013)
---------------------------
- NodeGuard now fully supported
- It's easy now to simulate delayed answers from partiuclar nodes or of
  particular type -- any response generated from CElmb class has argument
  of delay that has to pass after which it'll be passed to the CAN party

