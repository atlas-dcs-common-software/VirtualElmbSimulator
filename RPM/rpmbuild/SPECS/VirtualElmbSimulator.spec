%define version 1.3.6
# Author + Responsible:  Piotr Nikiel
%define name VirtualElmbSimulator 
%define release 0 
%define _topdir %(echo $PWD)
%define _tmpdir %{_topdir}/tmp
%define PREFIX /opt/%{name}

#AutoReqProv: no 
Summary: Virtual ELMB Simulator - standalone version (with socketcan interface) + CanOpenOpcUaServer hardware component 
Name: %{name}
Version: %{version}
Release: %{release}
Source0: checkout.tar 
License: CERN(to be updated) 
Group: CERN 
BuildArch: x86_64
BuildRoot: %{_topdir}/BUILDROOT/%{name}-%{version}
Vendor: atlas-dcs@cern.ch
Prefix: %{PREFIX}
Requires: boost-signals boost-thread

%description
Virtual ELMB Simulator - standalone version (with socketcan interface) + CanOpenOpcUaServer hardware component 


%prep
echo %{_topdir}
%setup -n checkout

%build
cmake28 .
make

%install

mkdir -p %{buildroot}/%{PREFIX}
cp README.txt %{buildroot}/%{PREFIX}
cp TheVirtualElmbSimulator.html %{buildroot}/%{PREFIX}
cd CoreSimulator
cp CoreSimulator.xsd %{buildroot}/%{PREFIX}
cd ../libsimulatedcan
cp libsimulatedcan.so %{buildroot}/%{PREFIX}
cd ../SocketCan
cp SocketCan sim_config.xml %{buildroot}/%{PREFIX}

# INSTALL_ROOT=%{buildroot} make install

%pre
echo "Pre-install: nothing to do"

%post
echo "Post-install: creating ld.so information and running ldconfig ..."
rm -f /etc/ld.so.conf.d/%{name}.conf
echo %{PREFIX} > /etc/ld.so.conf.d/%{name}.conf
ldconfig


%preun

%postun

if [ $1 = 0 ]; then
	echo "Post-uninstall: removing ld.so information "
	rm -f /etc/ld.so.conf.d/%{name}.conf

fi

# Force ldconfig irrespectively whether any .so information was added/removed 
ldconfig

%clean
# On intention empty, because after rpmbuild we want to package the same
# output files as tarball to.
echo "cleaning"

%files
%defattr(-,root,root)
%{PREFIX}

%changelog
