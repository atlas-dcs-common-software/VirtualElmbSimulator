#!/bin/bash
rm -Rf checkout

git clone https://:@gitlab.cern.ch:8443/atlas-dcs-common-software/VirtualElmbSimulator.git -b VirtualElmbSimulator-$1 --depth=1 checkout

RPM_VERSION=$1
mkdir -p rpmbuild/SOURCES rpmbuild/BUILD rpmbuild/BUILDROOT
rm -f rpmbuild/SOURCES/checkout.tar
rm -f rpmbuild/SPECS/VirtualElmbSimulator.spec
rm -Rf rpmbuild/BUILD/*
rm -Rf rpmbuild/BUILDROOT/*
tar cf rpmbuild/SOURCES/checkout.tar checkout/
echo "%define version $RPM_VERSION" > rpmbuild/SPECS/VirtualElmbSimulator.spec
cat checkout/VirtualElmbSimulator.spec.template >> rpmbuild/SPECS/VirtualElmbSimulator.spec
rm -Rf checkout
cd rpmbuild
rpmbuild -bb SPECS/VirtualElmbSimulator.spec
