/*
 * CUtils.h
 *
 *  Created on: Jun 18, 2013
 *      Author: pnikiel
 */

#ifndef CUTILS_H_
#define CUTILS_H_

#include <sys/time.h>
#include <string>
#include <sstream>
#include "simulated.h"

namespace SimulatedCan {

class CUtils {
public:
	static double getTimeDiff (timeval *t0, timeval *t1);
	/* Checks if time interval between lastTime and 'now' is at least of interval. If not returns false and changes nothing. If yes then returns true and also adjusts lastTime to 'now' */
	static bool checkInterval (timeval *lastTime, double interval);
	static void delaySeconds (float s);
	static void printCanFrame (const can_frame &f, bool force=false);
	static std::string canFrameToString (const can_frame &f);

	static int portShift;
	static void remapCanPort (const std::string& in, std::string& out);

	/* Template - implementation can't go to .cpp */
	template<typename T>
	static std::string toString(const T& v)
	{
	    std::ostringstream oss;
	    oss << v;
	    return oss.str();
	}


	static void byteToHex (unsigned char c, std::string &out);
	static unsigned char hexToByte( std::string in );

};

} /* namespace SimulatedCan */
#endif /* CUTILS_H_ */
