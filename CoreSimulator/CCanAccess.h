/*
 * CCanAccess.h
 *
 *  Created on: Apr 4, 2011
 *      Author: vfilimon
 */

#ifndef CCANACCESS_H_
#define CCANACCESS_H_

#include <time.h>
#include "boost/bind.hpp"
#include "boost/signals2.hpp"

#ifdef WIN32
#include "Winsock2.h"
#endif
#include <string>

typedef struct CanMsgStruct
{
	long c_id;
	unsigned char c_ff;
	unsigned char c_dlc;
	unsigned char c_data[8];
	timeval	c_time;
} canMessage;

class CCanAccess {
public:
	CCanAccess() {};
	virtual bool createBUS(const char * ,const char *) = 0 ;
	virtual bool sendRemoteRequest(short ) = 0;
	virtual bool sendMessage(short , unsigned char, unsigned char *) = 0;

	std::string& getBusName() { return m_sBusName; }

	void setBusName(std::string &name) {  m_sBusName = name;  }
	
	virtual ~CCanAccess() {};

	boost::signals2::signal<void (const canMessage &) > canMessageCame;
	boost::signals2::signal<void (const int,const char *,timeval &) > canMessageError;

private:
	std::string m_sBusName;
};

#endif /* CCANACCESS_H_ */
