/*
 * SdoSystem.cpp
 *
 *  Created on: Oct 26, 2015
 *      Author: pnikiel
 */

#include "SdoSystem.h"
#include <iostream>
#include <boost/thread/lock_guard.hpp>
#include "CUtils.h"

using SimulatedCan::CUtils;

SdoSystem::SdoSystem(boost::function<void(SdoReplyFrame)> replySender):
	m_replySender (replySender),
	mSegmentedSdoStoreDownloaded(false)
{

}

SdoSystem::~SdoSystem()
{
	// TODO Auto-generated destructor stub
}

bool SdoSystem::handle(const can_frame& f)
{
	unsigned int odIndex = (unsigned int)f.data[1] + ((unsigned int)f.data[2] << 8);
	unsigned int subIndex = (unsigned int)f.data[3];

	switch (f.data[0] & 0xe0) /* fetch the SDO command */
	{


	case 0x00: // download domain segment
	{
		if (m_lastObject)
		{
			auto reply = m_lastObject->downloadDomainSegment(f);
			if (reply.is_initialized())
							m_replySender( reply.get() );
			return true;
		}
		else
			return false;
	}
	case 0x20: // initiate domain download
	{
		LOG(Log::INF) << "initiate domain download";
		std::shared_ptr<SdoObject> object = findObject(odIndex, subIndex);
		if (object)
		{
			m_lastObject = object;
			auto reply = object->initiateDomainDownload (f);
			if (reply.is_initialized())
				m_replySender( reply.get() );
			return true;
		}
		else
			return false;
	}; break;

	case 0x40: // initiate domain upload
	{
		std::cout << "initiate domain upload" << std::endl;
		std::shared_ptr<SdoObject> object = findObject(odIndex, subIndex);
		if (object)
		{
			m_lastObject = object;
			auto reply = object->initiateDomainUpload (f);
			if (reply.is_initialized())
				m_replySender( reply.get() );
			else
			{
				LOG(Log::WRN) << "object found but no answer to be sent.";
			}
			return true;
		}
		else
		{
			LOG(Log::WRN) << "no SDO object registered for this request";
			return false;
		}

	}; break;

	case 0x60: // upload domain segment
	{
		if (m_lastObject)
		{
			auto reply = m_lastObject->uploadDomainSegment(f);
			if (reply.is_initialized())
				m_replySender( reply.get() );
			else
			{
				LOG(Log::WRN) << "object found but no answer to be sent.";
			}
			return true;
		}
		else
			return false;

		//todo
		LOG(Log::ERR) << "upload domain segment not supported";
		return false;

	}

	default:
		return false;
	}
}



std::shared_ptr<SdoObject> SdoSystem::findObject(unsigned int objectNumber, unsigned char subObjectNumber)
{
	for (auto object : m_objects)
	{
		if (object->matchesIds(objectNumber, subObjectNumber))
			return object;
	}
	return 0;
}

void SdoSystem::enableLoggingSegmented(const std::string& fileName)
{
	mSegmentedSdoStoreFileName = fileName;
	mSegmentedSdoStoreDownloaded = true;
}

void SdoSystem::logSegmentedTransfer(const CElmbSegmentedSdoTransferData& data)
{
	boost::lock_guard<decltype (mSegmentedSdoMutex)> l(mSegmentedSdoMutex);
	mSegmentedSdoDownloadTransfers.push_back(data);

}

void SdoSystem::writeStoredSegmentedTransfersToFile()
{
	boost::lock_guard<decltype (mSegmentedSdoMutex)> l(mSegmentedSdoMutex);
	std::ofstream f;
	f.open(mSegmentedSdoStoreFileName.c_str(), std::fstream::app | std::fstream::out);

	for (auto& transfer: mSegmentedSdoDownloadTransfers)
		writeOneTransfer(f, transfer);
	mSegmentedSdoDownloadTransfers.clear();
	f.close();

}

void SdoSystem::writeOneTransfer(ofstream& f,
		const CElmbSegmentedSdoTransferData& xfer)
{
	std::string registerName;
	std::string tmp;
	switch (xfer.odIndex())
	{
	case 0x480a: registerName.assign("Ireg_wr"); break;
	case 0x480b: registerName.assign("Dreg_wr"); break;
	default: registerName.assign (CUtils::toString(xfer.odIndex()));
	}
	f << std::dec << registerName << ":";
	for (int i=0; i<xfer.data().size(); i++)
	{
		CUtils::byteToHex(xfer.data()[i], tmp);
		f << tmp;
		if (i < xfer.data().size()-1)
			f << ",";
	}
	f << std::endl;
}
