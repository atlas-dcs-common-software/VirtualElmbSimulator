/*
 * CCanBus.h
 *
 *  Created on: Jun 17, 2013
 *      Author: pnikiel
 */

#ifndef CCANBUS_H_
#define CCANBUS_H_

#include "CElmb.h"
#include <list>

namespace SimulatedCan {


class CCanBus {
public:
	CCanBus ();
	~CCanBus ();
	void associateElmb (CElmb *elmb);
	void processFrame (can_frame &f);
	CElmb* findElmb (int nodeId) const;

	const std::list<CElmb*> getListElmbs() const;

	void assignName (std::string name);
	const std::string& getName () const;

	void setBusBroken (bool broken) { m_busBroken = broken; }
	bool getBusBroken () const { return m_busBroken; }

private:
	class CCanBusInternal; /* pimpl */
	CCanBusInternal *impl;
	std::string mBusName;
	bool m_busBroken;


};

} /* namespace SimulatedCan */
#endif /* CCANBUS_H_ */
