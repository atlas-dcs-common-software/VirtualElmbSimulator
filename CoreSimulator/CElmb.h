/*
 * CElmb.h
 *
 *  Created on: Jun 17, 2013
 *      Author: Piotr Nikiel <pnikiel>
 */

#ifndef CELMB_H_
#define CELMB_H_

#include "simulated.h"
#include "CCanFrameSender.h"
#include "CoreSimulator.hxx"

namespace SimulatedCan
{


class CElmb {
public:
	CElmb (const ::Node & xmlConfigNode);
	unsigned int getNodeId ();
	void processFrame (const can_frame &f);
	void setSerialByBusNode (unsigned int bus, unsigned int node);
	unsigned int yield ();
	void setCanFrameSender (CCanFrameSender *s);
	unsigned int getNmtState ();
	void setAnalogInput (unsigned int channel, unsigned int value);
	void setConversionFlag (unsigned int channel, unsigned int flag);

	void setDigitalInput (std::string port, unsigned char value);
	unsigned char getDigitalInput (std::string port);

	void getArrayRegister (std::string regName, unsigned int byteFrom, unsigned int byteNum, unsigned char* out);
	unsigned int getCountReceivedRtrTPDO3 ();
	unsigned int getCountReceivedSync ();
	unsigned int getCountReceivedNodeGuard ();
	void sendBootup ();
	void writeSegmentedSdoToFile ();
	void sendEmergencyObject (unsigned int errorCode, unsigned int errorReg, unsigned int byte1=0, unsigned int byte2=0, unsigned int byte3=0 );
	void setCanConnectivityEnabled (bool b);

	/* Test buffer access */
	void setTestBuffer9999( std::string in );
	std::string getTestBuffer9999();

private:
	void operator=(const CElmb&);
	CElmb (const CElmb&);

	class CElmbInternal; /* pimpl */
	CElmbInternal *impl;
	bool canConnectivityEnabled;

};

};


#endif /* CELMB_H_ */
