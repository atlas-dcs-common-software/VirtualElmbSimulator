/*
 * CElmbFrameFactory.h
 *
 *  Created on: Jun 17, 2013
 *      Author: pnikiel
 */

#ifndef CELMBFRAMEFACTORY_H_
#define CELMBFRAMEFACTORY_H_

#include "simulated.h"

namespace SimulatedCan {

class CElmbFrameFactory {
public:
	static void makeNmtSlaveNodeGuard (can_frame *f, unsigned int node, bool toggle, unsigned int state);
	static void makeTpdo3 (can_frame *f, unsigned int node, unsigned int channel, unsigned int adcValueUv, unsigned char flag=0x7a);
	static void makeTpdo1 (can_frame *f, unsigned int node, unsigned char portA, unsigned char portC, unsigned char portF);
	static void makeBootup (can_frame *f, unsigned int node);
};

} /* namespace SimulatedCan */
#endif /* CELMBFRAMEFACTORY_H_ */
