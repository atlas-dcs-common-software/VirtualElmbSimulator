/*
 * CMonitor.h
 *
 *  Created on: Jun 18, 2013
 *      Author: pnikiel
 */

#ifndef CMONITOR_H_
#define CMONITOR_H_

#include <boost/thread/mutex.hpp>
#include <vector>

namespace SimulatedCan {

struct CMonitorInfo {
	int yieldRate;
	int channelYieldRate;
	std::string mBusName;
};

class CMonitor {
public:
	static CMonitor* getInstance ();
	CMonitor();
	int obtainHandle ();
	void setBusName (int handle, std::string busName);
	void updateInfo (int handle, int yield, int channelYield);
	void redraw ();

private:
	static CMonitor * mInstance;
	static boost::mutex mMutex;
	int handles;
	std::vector<CMonitorInfo> infos;
	char lastSymbol;
	timeval lastRedraw;
	double redrawInterval;

};

} /* namespace SimulatedCan */
#endif /* CMONITOR_H_ */
