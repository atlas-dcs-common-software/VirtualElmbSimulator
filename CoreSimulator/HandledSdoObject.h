/*
 * HandledSdoObject.h
 *
 *  Created on: Oct 27, 2015
 *      Author: pnikiel
 *
 *  This class lets install handlers to source/sink data for SDO transmission.
 *  Transfer mechanism will be picked automatically (judging on the data size) unless forced.
 */

#ifndef CORESIMULATOR_HANDLEDSDOOBJECT_H_
#define CORESIMULATOR_HANDLEDSDOOBJECT_H_

#include <vector>

#include "SdoObject.h"
#include <boost/function.hpp>

class NoSuchExpeditedSdoObject: public std::exception {};

struct SdoData
{
	std::vector<unsigned char> data;
	SdoData () {}
	SdoData (unsigned char d): data{d} {}
	SdoData (unsigned char d1, unsigned char d2): data{d1, d2}  {}
	SdoData (unsigned char d1, unsigned char d2, unsigned char d3): data{d1, d2, d3} {}
	SdoData (unsigned char d1, unsigned char d2, unsigned char d3, unsigned char d4): data{d1, d2, d3, d4} {}
	SdoData (const unsigned char* b, size_t s): data(b, b+s) {  }
	SdoData (const std::vector<unsigned char>&v): data{v} {}

	std::vector<unsigned char> asVector () const { return data;}

	size_t length() const {return data.size();}
};

class HandledSdoObject: public SdoObject
{
public:
	HandledSdoObject( unsigned int objectNumber, unsigned char subObjectNumber, boost::function<SdoData()> readHandler, boost::function<void(SdoData)> writeHandler);
	HandledSdoObject( unsigned int objectNumber, boost::function<SdoData()> readHandler, boost::function<void(SdoData)> writeHandler);
	virtual ~HandledSdoObject();


	virtual boost::optional<SdoReplyFrame> initiateDomainDownload (const can_frame &f );
	virtual boost::optional<SdoReplyFrame> initiateDomainUpload (const can_frame &f);
	virtual boost::optional<SdoReplyFrame> downloadDomainSegment (const can_frame &f);
	virtual boost::optional<SdoReplyFrame> uploadDomainSegment (const can_frame &f);

	virtual SdoData read( );

	//! 'Domain download' in other terms. If handler is assigned, will route to the handler
	virtual bool write( SdoData data);

private:
	boost::function<void (SdoData)> m_writeHandler;
	boost::function<SdoData()> m_readHandler;
	bool m_isSegmentedOngoing;
	std::vector<unsigned char> m_segmentedDataBuffer;
	unsigned int m_done;
	bool mSdoLastToggleBitRx;
	bool mSdoNextToggleBitTx;
	bool mSdoUploadNextToggleBitTx;
};

#endif /* CORESIMULATOR_HANDLEDSDOOBJECT_H_ */
