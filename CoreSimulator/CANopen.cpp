/*
 * CANopen.cpp
 *
 *  Created on: Nov 22, 2013
 *      Author: dcs
 */


#include "CANopen.h"

const char* CANopen_getNmtStateText (unsigned int nmt)
{
	switch (nmt)
	{
	case NMT_STATE_INITIALISING: return "initialising"; break;
	case NMT_STATE_OPERATIONAL: return "operational"; break;
	case NMT_STATE_PREOPERATIONAL: return "preoperational"; break;
	case NMT_STATE_STOPPED: return "stopped"; break;
	default: return "?";

	}
}
