/* 
 * Created 1 Jun 2013
 * Author 	Piotr Nikiel <pnikiel>
 *
 */

#include "CElmb.h"
#include "CElmbFrameFactory.h"
#include "CUtils.h"
#include "CANopen.h"
#include "CDelayGenerator.h"

#include "iostream"
#include <string.h>
#include <sys/time.h>
#include <stdlib.h>
#include <fstream>
#include <list>
#include <boost/thread/mutex.hpp>
#include <boost/bind.hpp>
#include "HandledSdoObject.h"
#include "SdoSystem.h"
#include "DigitalSystem.h"
#include <memory>
using namespace std;


extern bool printDebugs; /* TODO get rid of this hsit */

namespace SimulatedCan
{

class CElmbSegmentedSdoTransferData {
public:
	std::vector<char> data;
	unsigned int odIndex; /* Object dictionary index to which this transfer wrote to */
};

class CElmb::CElmbInternal /* Not exposed outside cpp file */
{
public:
    CElmbInternal(const ::Node& xmlConfigNode);
    void processRpdo1 (const can_frame &f);
    void processRpdo4 (const can_frame &f);
    void processRtrTPDO3 ();
    void processRtrTPDO4 ();
    void processNmt (const can_frame &f);


    void processSdoFromClientStoreParameters (const can_frame &f);
    void processSdoFromClientRestoreDefaultParameters (const can_frame &f);
    void sendSdoResponse (const can_frame &src, unsigned char *resp, unsigned int respLen);
    unsigned int yield();
    unsigned int mNodeId;
    unsigned int mLastUpdatedChannel;
    unsigned int mLastValue;
    unsigned int mNumChannels;
    char mSerialString[4];

    const char* getSerialNumber () { return mSerialString; }

    CCanFrameSender *mReplySender;
    void sendResponse(can_frame &f, double d);
    /* NMT */
    unsigned int mNmtState;
    unsigned int getNmtState ();
    double mInitialisationTime;
    timeval mCpuBootupTime;
    void sendBootup ();
    void startNode ();
    void stopNode ();
    void enterPreoperational ();
    void reset ();

    enum {TRANSMISSION_CONSTANT_THROUGHPUT, TRANSMISSION_SYNC, TRANSMISSION_ASYNC} transmissionType;
    /* SYNC-based operation variables */
    double syncInterval;
    timeval syncLastSendout;
    int mSyncNumToGo;
    void sync ();
    void nodeGuard ();
    bool lastToggleState;
    unsigned int mCurrentAnalogValue;
	/* ASYNC-based operation variables */
	/* Constant based operation */
	timeval mConstantThroughputLast;
    double mConstantThroughputInterval;

	void sendDigitalInputsPDO ();
	/* SDO Operation */
    double mSdoDelay;

    void setDigitalInput (std::string port, unsigned char value);
    unsigned char getDigitalInput (std::string port);
    

	CDelayGenerator mSegmentedSdoDelay;
	char mDoInitHigh;

	void writeStoredSegmentedTransfersToFile ();

	/* Analog operation */
	enum {ANALOG_INPUTS_INCREMENT, ANALOG_INPUTS_RANDOMISE, ANALOG_INPUTS_EXTERNAL} analogInputsSyncAction;
	unsigned int mAnalogExternalInputs [64]; /* when external used */
	void setAnalogInput (unsigned int channel, unsigned int value);
	void setConversionFlag (unsigned int channel, unsigned int flag);

	// Following bytes are understood as documented by Henk in his object dictionary
	uint8_t m_analogRate;   // OD 2100/2
	uint8_t m_analogRange;  // OD 2100/3
	uint8_t m_analogMode;   // OD 2100/4


	/* MDT (Muon) specific ELMB things */
	/* Ireg, Dreg are special additiond to simulate MDT ELMB specific features used for JTAG and handled mostly by Segmented SDO. */
	std::vector<unsigned char> m_Ireg;
	std::vector<unsigned char> m_Dreg;

	void getArrayRegister (std::string regName, unsigned int byteFrom, unsigned int byteNum, unsigned char* out);

	/* Random Frame Generator things */
	double mRandomFrameGeneratorRate;
	timeval mRandomFrameGeneratorLast;
	unsigned int mRandomFrameGeneratorSeed;
	void sendRandomFrame () ;

	/* Counters */
	unsigned int mCounterReceivedRtrTPDO3;
	unsigned int getCountReceivedRtrTPDO3() {return mCounterReceivedRtrTPDO3; }
	unsigned int mCounterReceivedSync;
	unsigned int getCountReceivedSync() { return mCounterReceivedSync; }
	unsigned int mCounterReceivedNodeGuard;
	unsigned int getCountReceivedNodeGuard () { return mCounterReceivedNodeGuard; }

	/* Emergency objects */
	void sendEmergencyObject (unsigned int errorCode, unsigned int errorReg=0, unsigned int byte1=0, unsigned int byte2=0, unsigned int byte3=0 );

	/* Test object 9999 */
	unsigned char m_testBuffer9999[4];

	/* Test buffer access */
	void setTestBuffer9999( std::string in );
	void setTestBuffer9999( const unsigned char *ptr, size_t len)
	{
		std::copy (ptr, ptr+std::min(sizeof m_testBuffer9999, len), m_testBuffer9999);
	}
	std::string getTestBuffer9999();

	unsigned char m_analogFlags[64];

	SdoSystem m_sdoSystem;
	DigitalSystem m_digitalSystem;

	void sendSdoReply (SdoReplyFrame f);

	unsigned char m_TPDO2_transmissionType;
	unsigned char m_TPDO2_eventTimer;
};

SdoData nullReadHandler(unsigned int) { throw NoSuchExpeditedSdoObject();}

void nullWriteHandler( SdoData ) { throw NoSuchExpeditedSdoObject(); }

CElmb::CElmbInternal::CElmbInternal(const ::Node& xmlConfigNode):
		mLastUpdatedChannel(0),
		mLastValue(0),
		mNodeId(xmlConfigNode.id()),
		mNumChannels(64),
		mReplySender(0),
		mSdoDelay (xmlConfigNode.sdoDelay()),
		mSegmentedSdoDelay (xmlConfigNode.segmentedSdoDelay()),
		mInitialisationTime(3.0),
//		mSdoReceptionOpen(false),
		mRandomFrameGeneratorRate (xmlConfigNode.randomFrameGeneratorRate ()),
		mCounterReceivedRtrTPDO3(0),
		m_sdoSystem(boost::bind(&CElmbInternal::sendSdoReply, this, _1)),
//		mSdoReceivedBytes(0),
		m_Ireg{0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07},
		m_analogMode (1),
		m_analogRange (4),
		m_analogRate (1)

{
    gettimeofday (&mCpuBootupTime, 0);
    gettimeofday (&mRandomFrameGeneratorLast, 0);
    mRandomFrameGeneratorSeed = mRandomFrameGeneratorLast.tv_usec;
    if (xmlConfigNode.transmissionType().compare("sync")==0)
	transmissionType = TRANSMISSION_SYNC;
    else if (xmlConfigNode.transmissionType().compare("constant_throughput")==0)

    {
	transmissionType = TRANSMISSION_CONSTANT_THROUGHPUT;
	if (xmlConfigNode.constantThroughput() != 0.0)
	{
	    mConstantThroughputInterval  = 1.0 / xmlConfigNode.constantThroughput();
	}
	else
	    mConstantThroughputInterval = 1E9;
	cout << "ConstantThroughput interval=" << mConstantThroughputInterval << endl;
	gettimeofday (&mConstantThroughputLast, 0);
    }
    if (xmlConfigNode.analogInputsSyncAction().compare("randomise") == 0)
	analogInputsSyncAction = ANALOG_INPUTS_RANDOMISE;
    else if (xmlConfigNode.analogInputsSyncAction().compare("external") == 0)
	analogInputsSyncAction = ANALOG_INPUTS_EXTERNAL;
    else
	analogInputsSyncAction = ANALOG_INPUTS_INCREMENT;

    strncpy (mSerialString, xmlConfigNode.serial().c_str(), sizeof mSerialString);

    reset ();
    if (xmlConfigNode.initialNmtState().compare("started") == 0)
	startNode();
    else if (xmlConfigNode.initialNmtState().compare("stopped") == 0)
	stopNode();
    else if (xmlConfigNode.initialNmtState().compare("reseted") == 0)
    { /* already resetted no reason to do anything */}

    double convRate=xmlConfigNode.conversionRate();
    if (convRate > 1.00)
	syncInterval = 1.0 / convRate;
    else
	syncInterval = 1.0;


    if (xmlConfigNode.segmentedSdoStoreFileName() != "")
    {
	m_sdoSystem.enableLoggingSegmented(xmlConfigNode.segmentedSdoStoreFileName());
    }

    /* Here we create SDO objects -> in future find a function for that */
    /* 1000/xx Device type */
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x1000, [](){return SdoData(0x91, 0x01, 0x0f, 0x00);}, nullWriteHandler));

    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x1009, [](){return SdoData('e', 'l', '4', '0');}, nullWriteHandler));

    /* 100a/xx SW Version */
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x100a, 0x00, [](){return SdoData('M', 'A', '4', '4');}, nullWriteHandler));
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x100a, 0x01, [](){return SdoData('0', '0', '0', '1');}, nullWriteHandler));

    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x100c, [](){return SdoData(1000>>8, 1000&0xff);}, nullWriteHandler));

    /* 100d/xx Lifetime factor */
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x100d, [](){return SdoData(70);}, nullWriteHandler));

	/* 1010 SAVE */
	m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x1010, 0x01, [](){return SdoData(1, 0, 0, 0);}, nullWriteHandler));

    /* 2100/xx ADC config */
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2100, 0x01, [](){return SdoData(64);}, [](const SdoData&){} )); /* number of input channels */
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2100, 0x02, [this](){return SdoData(m_analogRate);},  [this](const SdoData &d){m_analogRate = d.data[0];} )); /* conversion word rate */
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2100, 0x03, [this](){return SdoData(m_analogRange);}, [this](const SdoData &d){m_analogRange = d.data[0];} )); /* input voltage range */
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2100, 0x04, [this](){return SdoData(m_analogMode);},  [this](const SdoData &d){m_analogMode = d.data[0];} )); /* bipolar/unipolar */


    /* 2200/xx Digital Input debounce timer */
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2200, [](){return SdoData(10);}, [](const SdoData&){} ));


    // TODO: why mDoInitHigh is this way?
    // 2300/xx Digital Output init
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2300, [](){return SdoData(1);}, [this](SdoData d){mDoInitHigh = d.data[0];}));


    // 3100/xx Serial Number
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x3100, [this](){return SdoData(reinterpret_cast<const unsigned char*>(getSerialNumber()), (size_t)4);}, nullWriteHandler));

    /* 1801/xx TPDO2 params */
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x1801, 0x02, [this](){return SdoData(m_TPDO2_transmissionType);}, [this](const SdoData& d){ m_TPDO2_transmissionType=d.data[0]; } )); // transmission type
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x1801, 0x05, [this](){return SdoData(m_TPDO2_eventTimer, 0);}, [this](const SdoData& d){ m_TPDO2_eventTimer=d.data[0]; } )); // event timer

    /* 1802/xx TPDO3 params */
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x1802, 0x02, [](){return SdoData(255);}, [](const SdoData&){} )); // transmission type
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x1802, 0x05, [](){return SdoData(1, 0);}, [](const SdoData&){} )); // event timer


    /* 1800/xx TPDO1 params */
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x1800, 0x00, [](){return SdoData(5);}, nullWriteHandler));
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x1800, 0x02, [](){return SdoData(1);}, [](const SdoData&){} )); // transmission type
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x1800, 0x05, [](){return SdoData(0);}, [](const SdoData&){} )); // event timer




    /* 6000/xx Digital Inputs */
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6000, 0x00, [](){return SdoData(2);}, nullWriteHandler));
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6000, 0x01, [this](){return SdoData(m_digitalSystem.getValue(PortId::PORT_F));}, nullWriteHandler));
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6000, 0x02, [this](){return SdoData(m_digitalSystem.getValue(PortId::PORT_A));}, nullWriteHandler));
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6000, 0x03, [this](){return SdoData(m_digitalSystem.getValue(PortId::PORT_C));}, nullWriteHandler));



    /* INTERRUPT MASK ANY CHANGE */
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6006, 0x00, [](){return SdoData(2);}, nullWriteHandler));
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6006, 0x01, [this](){return SdoData(m_digitalSystem.getInterruptMask(PortId::PORT_F));}, [this](const SdoData &d){m_digitalSystem.setInterruptMask(PortId::PORT_F, d.data[0]);} ));
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6006, 0x02, [this](){return SdoData(m_digitalSystem.getInterruptMask(PortId::PORT_A));}, [this](const SdoData &d){m_digitalSystem.setInterruptMask(PortId::PORT_A, d.data[0]);} ));
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6006, 0x03, [this](){return SdoData(m_digitalSystem.getInterruptMask(PortId::PORT_C));}, [this](const SdoData &d){m_digitalSystem.setInterruptMask(PortId::PORT_C, d.data[0]);} ));


    /* 6200/xx Write Output State */
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6200, 0x00, [](){return SdoData(2);}, nullWriteHandler));
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6200, 0x01, [this](){return SdoData(m_digitalSystem.getValue(PortId::PORT_C));}, [this](const SdoData &d){m_digitalSystem.setValue(PortId::PORT_C, d.data[0]);} ));
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6200, 0x02, [this](){return SdoData(m_digitalSystem.getValue(PortId::PORT_A));}, [this](const SdoData &d){m_digitalSystem.setValue(PortId::PORT_A, d.data[0]);} ));
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6200, 0x03, [this](){return SdoData(m_digitalSystem.getValue(PortId::PORT_F));}, [this](const SdoData &d){m_digitalSystem.setValue(PortId::PORT_F, d.data[0]);} ));

    /* 6208/xx Filter Mask Digital Ports */
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6208, 0x00, [](){return SdoData(2);}, nullWriteHandler));
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6208, 0x01, [this](){return SdoData(m_digitalSystem.getOutputMask(PortId::PORT_C));}, [this](const SdoData &d){m_digitalSystem.setOutputMask(PortId::PORT_C, d.data[0]);} ));
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6208, 0x02, [this](){return SdoData(m_digitalSystem.getOutputMask(PortId::PORT_A));}, [this](const SdoData &d){m_digitalSystem.setOutputMask(PortId::PORT_A, d.data[0]);} ));
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6208, 0x03, [this](){return SdoData(m_digitalSystem.getOutputMask(PortId::PORT_F));}, [this](const SdoData &d){m_digitalSystem.setOutputMask(PortId::PORT_F, d.data[0]);} ));

    /* 6220/xx Bit-based Digital Output control */
    for (int i=0; i<8; i++) // PORT C
	m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6220, 0x01 + i,
							     [this, i](){return SdoData(m_digitalSystem.getValueByBit(PortId::PORT_C, i));},
							     [this, i](const SdoData &d){m_digitalSystem.setValueByBit(PortId::PORT_C, i, d.data[0]);} ));
    for (int i=0; i<8; i++) // PORT A
	m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6220, 0x09 + i,
							     [this, i](){return SdoData(m_digitalSystem.getValueByBit(PortId::PORT_A, i));},
							     [this, i](const SdoData &d){m_digitalSystem.setValueByBit(PortId::PORT_A, i, d.data[0]);} ));

    for (int i=0; i<8; i++) // PORT F
	m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6220, 0x11 + i,
							     [this, i](){return SdoData(m_digitalSystem.getValueByBit(PortId::PORT_F, i));},
							     [this, i](const SdoData &d){m_digitalSystem.setValueByBit(PortId::PORT_F, i, d.data[0]);} ));
	
    
	
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x9999, [this](){return SdoData(m_testBuffer9999, 4);}, [this](const SdoData &d){ setTestBuffer9999(&d.data[0], d.data.size());  } ));

    // 6005/xx Global Digital Interrupts Enable
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x6005, [this](){return SdoData(0);},  [](const SdoData&){} ));

    /* OBJECTS SPECIFIC TO MDT ELMB - used i.e. in segmented sdo tests */
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2201, [](){return SdoData(0x69);}, [](const SdoData&){} )); //csm_reset_calib
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2101, [](){return SdoData(0x69);}, [](const SdoData&){} )); //csm_mezzmask

    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2600, [](){return SdoData(0x69);}, [](const SdoData&){} )); //bfReset0
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2601, [](){return SdoData(0x69);}, [](const SdoData&){} )); //bfReset1
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2602, [](){return SdoData(0x69);}, [](const SdoData&){} )); //bfReset2
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2603, [](){return SdoData(0x69);}, [](const SdoData&){} )); //bfReset3


    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2900, 0x01, [](){return SdoData(0x69);}, nullWriteHandler )); //bfIfH0
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2900, 0x02, [](){return SdoData(0x69);}, nullWriteHandler )); //bfIdL0
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2901, 0x01, [](){return SdoData(0x69);}, nullWriteHandler )); //bfIfH1
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2901, 0x02, [](){return SdoData(0x69);}, nullWriteHandler )); //bfIdL1
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2902, 0x01, [](){return SdoData(0x69);}, nullWriteHandler )); //bfIfH2
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2902, 0x02, [](){return SdoData(0x69);}, nullWriteHandler )); //bfIdL2
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2903, 0x01, [](){return SdoData(0x69);}, nullWriteHandler )); //bfIfH3
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x2903, 0x02, [](){return SdoData(0x69);}, nullWriteHandler )); //bfIdL3


    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x480a, [this](){return SdoData(m_Ireg );},  [this](const SdoData &d){m_Ireg = d.asVector() ;} ));
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x480b, [this](){return SdoData(m_Dreg );},  [this](const SdoData &d){m_Dreg = d.asVector() ;} ));
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x480c, [this](){return SdoData(m_Ireg );}, nullWriteHandler  ));

    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x4840, [](){return SdoData(0x69);}, [](const SdoData&){} )); //tapReset
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x4860, [](){return SdoData(0x69);}, nullWriteHandler )); //tckTiming

    // Store and restore parameters to/from EEPROM
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x1010, [](){return SdoData(0);}, [](const SdoData&){} ));
    m_sdoSystem.addObject(make_shared<HandledSdoObject> (0x1011, [](){return SdoData(0);}, [](const SdoData&){} ));


}

void CElmb::CElmbInternal::processRpdo1 (const can_frame &f)
{
	LOG(Log::TRC) << "RPDO1 has arrived " << hex << (unsigned int)f.can_dlc << " " << (unsigned int)f.data[0] << " " << (unsigned int)f.data[1] << " " << (unsigned int)f.data[2] << dec << endl;
	if (f.can_dlc > 0)
	{
		m_digitalSystem.setValue(PortId::PORT_C, f.data[0]);
		if (f.can_dlc > 1)
		{
			m_digitalSystem.setValue(PortId::PORT_A, f.data[1]);
			if (f.can_dlc > 2)
				m_digitalSystem.setValue(PortId::PORT_F, f.data[2]);
		}
	}
}


void CElmb::CElmbInternal::processRpdo4 (const can_frame &f)
{
	LOG(Log::TRC) << "RPDO4 has arrived";
	setTestBuffer9999( f.data, 4 );
}

void CElmb::CElmbInternal::processNmt(const can_frame &f)
{
	/* Reference CANOPEN 3.6.1 */
	/* format: byte0-CS, byte1-addressed node. */
	if (f.data[1]!=0 && f.data[1]!=mNodeId)
		return; /* not for us */

	switch (f.data[0])
	{
	case 1: /*start remote node */
		startNode ();
		break;
	case 2: /* stop remote node */
		stopNode ();
		break;
	case 128: /* enter preoperational */
		enterPreoperational();
		break;
	case 129:
		reset();
		break;
	default:
		cout << "processNmt(): unsupported command cs="<<f.data[0] << endl;

	}
}


void CElmb::CElmbInternal::writeStoredSegmentedTransfersToFile ()
{
	m_sdoSystem.writeStoredSegmentedTransfersToFile();
}


void createExpeditedSdoObjects ()
{
//	HandledExpeditedSdoObject a( 0x9009, nullReadHandler, nullWriteHandler );
	HandledSdoObject b( 0x1203, 0x01, [](){return SdoData(1);}, nullWriteHandler );

}


void CElmb::CElmbInternal::setAnalogInput (unsigned int channel, unsigned int value)
{
	if (channel>64) /* TODO channel number should be some sort of constant */
		return;
	mAnalogExternalInputs[channel] = value;
}

void CElmb::CElmbInternal::sendResponse(can_frame &f, double delay) {
	if (mReplySender == 0) {
		std::cout << "ELMB wants to send a reply, but handler is not registered!!!!" << std::endl;
	}
	else
	{
		if (printDebugs)
			std::cout << "sending response from elmb nodeid=" << mNodeId << std::endl;
		mReplySender->sendFrame(f, delay);
	}
}



void CElmb::CElmbInternal::sendDigitalInputsPDO ()
{
	LOG(Log::TRC) << "sendDigitalInputsPDO";
	//m_digitalSystem.setValue(PortId::PORT_F, rand());
	can_frame f;
	CElmbFrameFactory::makeTpdo1 (&f, mNodeId,
			m_digitalSystem.getValue(PortId::PORT_A),
			m_digitalSystem.getValue(PortId::PORT_C),
			m_digitalSystem.getValue(PortId::PORT_F));
	sendResponse (f, 0);
}

	

void CElmb::CElmbInternal::sync ()
{
	mCounterReceivedSync++;
	mCurrentAnalogValue++;
	if (printDebugs)
		std::cout << "SYNC received" << std::endl;
	mSyncNumToGo = mNumChannels+1; // +1 is for digital input PDO


	gettimeofday (&syncLastSendout, 0); 
	
}

void CElmb::CElmbInternal::nodeGuard () 
{
	mCounterReceivedNodeGuard++;
	can_frame f;
	CElmbFrameFactory::makeNmtSlaveNodeGuard (&f, mNodeId, lastToggleState, mNmtState);
	lastToggleState = !lastToggleState;
	sendResponse(f, 0);
}

void CElmb::CElmbInternal::sendBootup()
{
	can_frame f;
	CElmbFrameFactory::makeBootup(&f, mNodeId);
	sendResponse(f, 0);

}

void CElmb::CElmbInternal::startNode ()
{
	mNmtState = NMT_STATE_OPERATIONAL;
}

void CElmb::CElmbInternal::stopNode ()
{
	mNmtState = NMT_STATE_STOPPED;
}
void CElmb::CElmbInternal::enterPreoperational ()
{
	mNmtState = NMT_STATE_PREOPERATIONAL;
}
void CElmb::CElmbInternal::reset ()
{
	gettimeofday (&mCpuBootupTime, 0);
	mNmtState = NMT_STATE_INITIALISING;
	/* TODO: move all variables to default state */
	mCurrentAnalogValue = 0;
	lastToggleState = 0;
	mSyncNumToGo = 0;
}

unsigned int CElmb::CElmbInternal::yield ()
{
	/* NMT State business */
	if (mNmtState == NMT_STATE_INITIALISING)
	{
		if (CUtils::checkInterval(&mCpuBootupTime,mInitialisationTime))
		{
			/* Enough time passed to simulate elmb booting up. NMT State should change */
			/* Refer CANOPEN p.3.6.3 */
			mNmtState = NMT_STATE_PREOPERATIONAL;
			sendBootup();

		}

	}

	unsigned int returnVal;

	/* Below - all the business with simulating analog and digital operations */
	if (mNmtState == NMT_STATE_OPERATIONAL)
	{
		can_frame f;
		switch (transmissionType)
		{
		case TRANSMISSION_CONSTANT_THROUGHPUT:
			/* Check if proper time passed for an update */
			if (CUtils::checkInterval(&mConstantThroughputLast, mConstantThroughputInterval))
			{
				/* okay time to send new update */
				if (printDebugs)
					std::cout << "ELMB " << mNodeId << " yield channel " << mLastUpdatedChannel << std::endl;
				CElmbFrameFactory::makeTpdo3(&f, mNodeId, mLastUpdatedChannel, mLastValue, m_analogFlags[mLastUpdatedChannel]);
				sendResponse(f, 0);
				mLastUpdatedChannel = (mLastUpdatedChannel+1) % mNumChannels;
				if (mLastUpdatedChannel==0)
					mLastValue = (mLastValue+1) % 10000;
			}


			break;
		case TRANSMISSION_SYNC:
			/* First check if last sync is still active - if it is finished, just do nothing */
			if (mSyncNumToGo < 1)
				return 0;

			/* See if enough time passed to send another update */
			else if (CUtils::checkInterval (&syncLastSendout, syncInterval))
			{
				if (mSyncNumToGo == mNumChannels+1)
				{
					sendDigitalInputsPDO ();
					mSyncNumToGo--;
				}
				else
				{
					unsigned int value;
					if (analogInputsSyncAction==ANALOG_INPUTS_RANDOMISE)
						value=rand();
					else if (analogInputsSyncAction==ANALOG_INPUTS_EXTERNAL)
						value=mAnalogExternalInputs[mSyncNumToGo-1];
					else
						value=mCurrentAnalogValue;
					CElmbFrameFactory::makeTpdo3(&f, mNodeId, mSyncNumToGo-1, value, m_analogFlags[mSyncNumToGo-1]);
					sendResponse(f, 0);
					mSyncNumToGo--;
				}
				if (mSyncNumToGo == 0)
					if (analogInputsSyncAction==ANALOG_INPUTS_INCREMENT)
						mLastValue = (mLastValue+1) % 10000;
			} 
			break;
		default:
			cerr << "transmissionType unknown!!" << endl;
		}
	}

	if (mRandomFrameGeneratorRate > 0.0)
	{
		if (CUtils::checkInterval(&mRandomFrameGeneratorLast, 1.0 / mRandomFrameGeneratorRate))
		{
			sendRandomFrame();
		}
	}

	return mLastUpdatedChannel;
}

unsigned int CElmb::CElmbInternal::getNmtState()
{
	return mNmtState;
}

void CElmb::CElmbInternal::sendRandomFrame()
{

	can_frame f;

	f.can_dlc = rand_r(&mRandomFrameGeneratorSeed)%8;
	f.can_id = rand_r(&mRandomFrameGeneratorSeed)&0x0fff;
	for (int i=0; i < ( sizeof f.data/sizeof f.data[0]); i++)
		f.data[i] = rand_r(&mRandomFrameGeneratorSeed);
	sendResponse(f,0);


}

void CElmb::CElmbInternal::processRtrTPDO3()
{
	cout << "RTR received" << endl;
	mCounterReceivedRtrTPDO3++;
}

void CElmb::CElmbInternal::processRtrTPDO4()
{
	/* This should force sending out a TPDO4 */
	can_frame f;
	f.can_id = 0x480 + mNodeId;
	f.can_dlc = sizeof m_testBuffer9999;
	std::copy(m_testBuffer9999, m_testBuffer9999+std::min(sizeof m_testBuffer9999, sizeof f.data), f.data );
	sendResponse( f, 0 );
}

void CElmb::CElmbInternal::getArrayRegister (std::string regName, unsigned int byteFrom, unsigned int byteNum, unsigned char* out)
{

	if (regName.compare("Ireg_wr")==0)
	{
		if (byteFrom+byteNum > m_Ireg.size())
			throw std::runtime_error ("Bounds error");
		std::copy(m_Ireg.begin()+byteFrom, m_Ireg.begin()+byteFrom+byteNum, out);
	}
	else
		throw std::runtime_error ("Register unknown");
}

void CElmb::CElmbInternal::sendEmergencyObject (unsigned int errorCode, unsigned int errorReg, unsigned int byte1, unsigned int byte2, unsigned int byte3 )
{
	std::cout << "Sending emergency object, bytes:" << std::hex << byte1<<","<<byte2<<","<<byte3<<std::dec<<std::endl;
	can_frame f;
	memset (&f, 0, sizeof f);
	f.can_id = 0x080 + mNodeId;
	f.can_dlc = 8;
	f.data[1] = (errorCode & 0xff00) >> 8;
	f.data[0] = errorCode & 0xff;
	f.data[2] = errorReg;
	f.data[3] = byte1;
	f.data[4] = byte2;
	f.data[5] = byte3;
	sendResponse(f, 0);

}

void CElmb::CElmbInternal::setConversionFlag (unsigned int channel, unsigned int flag)
{
	if (channel < sizeof m_analogFlags)
		m_analogFlags[channel] = flag;
	else
		throw std::runtime_error("channel out of range");
}

/* Test buffer access */
void CElmb::CElmbInternal::setTestBuffer9999( std::string in )
{
	for (int i=0; i < sizeof m_testBuffer9999; i++)
	{
		unsigned char value = CUtils::hexToByte( in.substr(i*2, 2 ));
		m_testBuffer9999[i] = value;
	}

}
std::string CElmb::CElmbInternal::getTestBuffer9999()
{
	std::string s;
	for (int i=0; i<sizeof(m_testBuffer9999); i++)
	{
		std::string o;
		CUtils::byteToHex(m_testBuffer9999[i], o);
		s+=o;
	}
	return s;
}

CElmb::CElmb (const ::Node & xmlConfigNode):
		impl(new CElmbInternal(xmlConfigNode)),
		canConnectivityEnabled(true)
{
	cout << "Created ELMB nodeid="<<xmlConfigNode.id()<<endl;
};



void CElmb::processFrame (const can_frame &f)
{
	if (!canConnectivityEnabled)
		return;

	if (printDebugs)
	{
		cout << "processFrame on nid=" << impl->mNodeId << endl;
		cout << "frame:";
		CUtils::printCanFrame(f, true);
	}

	unsigned int node = f.can_id & 0x7f;
	if (node>0 && node != impl->mNodeId) {
		std::cout << "CElmb::processFrame: given frame to process, but nodeid doesn't match !!" << std::endl;
	}
	unsigned int function_code = (f.can_id >> 7) & 0x0f;
	unsigned int rtr = f.can_id & SIMULATED_RTR_FLAG;

	if (rtr==0)
	{
		switch (function_code) {
		case 0x00: impl->processNmt (f); break;
		case 0x01: impl->sync(); break;
		case 0x04: /* RPDO1 */ impl->processRpdo1 (f); break;
		case 0x0a: /* RPDO4 */ impl->processRpdo4 (f); break;
		case 0x0c: impl->m_sdoSystem.handle(f);

			break;


		default: std::cout << "processFrame(): function code " << function_code << "unknown. " << std::endl;
		}
	}
	else
	{
		/* This is a RTR */
		switch (function_code) {
		case 0x07: impl->processRtrTPDO3(); break;
		case 0x09: impl->processRtrTPDO4(); break;
		case 0x0e: impl->nodeGuard(); break;
		default: std::cout << "processFrame(): RTR=yes function code " << function_code << "unknown. " << std::endl;
		}
	}
}


/* Returns number of next channel that will be updated in next yield() call */
unsigned int CElmb::yield ()
{
	return impl->yield();
}

unsigned int CElmb::getNodeId()
{
	return impl->mNodeId;
}

void CElmb::setSerialByBusNode (unsigned int bus, unsigned int node)
{
	impl->mSerialString[0] = '0'+bus%10;
	impl->mSerialString[1] = '0'+(bus%100)/10;
	impl->mSerialString[2] = '0'+node%10;
	impl->mSerialString[3] = '0'+(node%100)/10;
}


void CElmb::setCanFrameSender(CCanFrameSender *c)
{
	impl->mReplySender = c;
}

unsigned int CElmb::getNmtState()
{
	return impl->getNmtState();
}

void CElmb::setAnalogInput (unsigned int channel, unsigned int value)
{
	return impl->setAnalogInput(channel,value);
}

void CElmb::getArrayRegister (std::string regName, unsigned int byteFrom, unsigned int byteNum, unsigned char* out)
{
	impl->getArrayRegister(regName, byteFrom, byteNum, out);
}

void CElmb::sendBootup ()
{
	impl->sendBootup();
}

void CElmb::writeSegmentedSdoToFile ()
{
	impl->writeStoredSegmentedTransfersToFile();
}

unsigned int CElmb::getCountReceivedRtrTPDO3 ()
{
	return impl->getCountReceivedRtrTPDO3();
}

unsigned int CElmb::getCountReceivedSync ()
{
	return impl->getCountReceivedSync();
}

unsigned int CElmb::getCountReceivedNodeGuard ()
{
	return impl->getCountReceivedNodeGuard();
}

void CElmb::sendEmergencyObject (unsigned int errorCode, unsigned int errorReg,  unsigned int byte1, unsigned int byte2, unsigned int byte3 )
{
	impl->sendEmergencyObject(errorCode, errorReg, byte1, byte2, byte3);
}

void CElmb::setCanConnectivityEnabled (bool b)
{
	canConnectivityEnabled = b;
}

/* Test buffer access */
void CElmb::setTestBuffer9999( std::string in )
{
	impl->setTestBuffer9999(in);
}
std::string CElmb::getTestBuffer9999()
{
	return impl->getTestBuffer9999();
}

void CElmb::setConversionFlag (unsigned int channel, unsigned int flag)
{
	impl->setConversionFlag( channel, flag );
}

void CElmb::CElmbInternal /* Not exposed outside cpp file */
::sendSdoReply(SdoReplyFrame f)
{
	can_frame frame = f.frame();
	if (f.isSegmented())
		sendResponse(frame, mSegmentedSdoDelay.getDelay());
	else
		sendResponse(frame, mSdoDelay);
}

void CElmb::CElmbInternal::setDigitalInput (std::string port, unsigned char value)
{
	if (port=="A")
		m_digitalSystem.setValue(PortId::PORT_A, value);
	else if (port=="C")
		m_digitalSystem.setValue(PortId::PORT_C, value);
	else if (port=="F")
		m_digitalSystem.setValue(PortId::PORT_F, value);
	else
		throw std::runtime_error ("no_such_port");
}

    void CElmb::setDigitalInput (std::string port, unsigned char value)
    {
	impl->setDigitalInput(port, value);
    }

    unsigned char CElmb::CElmbInternal::getDigitalInput (std::string port)
    {
	if (port=="A")
            return m_digitalSystem.getValue(PortId::PORT_A);
	else if (port=="C")
            return m_digitalSystem.getValue(PortId::PORT_C);
	else if (port=="F")
            return m_digitalSystem.getValue(PortId::PORT_F);
	else
            throw std::runtime_error ("no_such_port");

    }
    
    
    unsigned char CElmb::getDigitalInput (std::string port)
    {
        return impl->getDigitalInput(port);
    }
    

}
