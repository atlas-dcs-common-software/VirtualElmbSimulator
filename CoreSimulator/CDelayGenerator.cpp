/*
 * CDelayGenerator.cpp
 *
 *  Created on: Dec 13, 2013
 *      Author: dcs
 */

#include "CDelayGenerator.h"
#include <stdlib.h>
#include <sstream>
#include <stdexcept>

namespace SimulatedCan {

CDelayGenerator::CDelayGenerator(const std::string &params): delayType(DELAY_CONST), lower(1.0), upper(1.0)
{
	/* The rule is simple. The delay may be specified as a single number:
	 * "5.8"
	 * or uniform distribution
	 * "uniform 2 3"
	 */

	/* understand which sort we really get */
	std::istringstream ss (params);
	std::string firstWord;
	ss >> firstWord;
	if (firstWord.compare("uniform")==0)
	{
		/* we want uniform distribution */
		ss >> lower;
		ss >> upper;
		if (ss.fail())
			throw std::runtime_error("improper arguments of uniform delay");
		delayType = DELAY_UNIFORM;
	}
	else
	{
		/* okay so maybe a number expressing const delay?? */
		ss.clear();
		ss.seekg(0, std::ios::beg);
		ss >> lower;
		if (ss.fail())
			throw std::runtime_error("improper delay specified: not uniform, not number");
		upper=lower;
		delayType = DELAY_CONST;

	}



}

CDelayGenerator::~CDelayGenerator() {
	// TODO Auto-generated destructor stub
}

double CDelayGenerator::getDelay() const
{
	switch (delayType)
	{
	case DELAY_UNIFORM:
		return double(rand())*(upper-lower)/double(RAND_MAX) + lower;
	case DELAY_CONST:
	default: return lower;
	}

}

} /* namespace SimulatedCan */

