/*
 * SdoObject.cpp
 *
 *  Created on: Oct 27, 2015
 *      Author: pnikiel
 */

#include "SdoObject.h"


SdoObject::SdoObject(unsigned int objectNumber, unsigned char subObjectNumber):
	m_objectNumber( objectNumber ),
	m_subObjectNumber( subObjectNumber ),
	m_anySubObject (false),
	m_parent(nullptr)
{
}

SdoObject::SdoObject(unsigned int objectNumber):
	m_objectNumber (objectNumber),
	m_subObjectNumber (0),
	m_anySubObject (true),
	m_parent(nullptr)
{
}


SdoObject::~SdoObject()
{

}

bool SdoObject::matchesIds(unsigned int object, unsigned char subObject)
{
	if (m_anySubObject)
		return (object == m_objectNumber);
	else
		return (object == m_objectNumber && subObject == m_subObjectNumber);
}
