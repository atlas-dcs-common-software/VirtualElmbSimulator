/*
 * CCanBus.cpp
 *
 *  Created on: Jun 17, 2013
 *      Author: pnikiel
 */

#include "CCanBus.h"
#include <iostream>
#include <exception>
#include <stdexcept>

namespace SimulatedCan {

class CCanBus::CCanBusInternal
{
public:
	std::list<SimulatedCan::CElmb *> elmbs;
};

CCanBus::CCanBus():
		impl (new CCanBusInternal),
		m_busBroken (false)
{
}

CCanBus::~CCanBus()
{
	delete impl;
}

void CCanBus::associateElmb(CElmb *e) {
	impl->elmbs.push_back(e);
}


CElmb* CCanBus::findElmb(int node) const
{
	std::list<CElmb*>::iterator it;
	for (it=impl->elmbs.begin(); it!=impl->elmbs.end(); it++) {
		if ((*it)->getNodeId() == node)
		{
			return *it;
		}
	}
	throw ::std::runtime_error("Can't find such ELMB");
}

void CCanBus::processFrame (can_frame &f) {
	/* Find an ELMB with matching node id and let this ELMB process a frame addressed to it */
	unsigned int node = f.can_id & 0x7f; /* TODO Fix This Ugly ptr */
	std::list<CElmb*>::iterator it;
	for (it=impl->elmbs.begin(); it!=impl->elmbs.end(); it++) {
		if (0==node || (*it)->getNodeId() == node) 
		{
			(*it)->processFrame(f);
		}
	}
}

const std::list<CElmb*> CCanBus::getListElmbs() const
{
	return impl->elmbs;
}

void CCanBus::assignName (std::string name)
{
	mBusName.assign(name);
}
const std::string& CCanBus::getName () const
{
	return mBusName;

}

} /* namespace SimulatedCan */
