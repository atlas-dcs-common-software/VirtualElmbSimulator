/*
 * SdoSystem.h
 *
 *  Created on: Oct 26, 2015
 *      Author: pnikiel
 */

#ifndef CORESIMULATOR_SDOSYSTEM_H_
#define CORESIMULATOR_SDOSYSTEM_H_

#include <list>
#include <vector>


#include <boost/function.hpp>
#include <sys/socket.h>
#include <linux/can.h> // for can_frame
#include <memory>
#include <LogIt.h>
#include "SdoObject.h"
#include <fstream>
#include <boost/thread/mutex.hpp>
using std::ofstream;

class CElmbSegmentedSdoTransferData {
public:
	CElmbSegmentedSdoTransferData (unsigned int odIndex, const std::vector<unsigned char> & data):
		m_data(data),
		m_odIndex(odIndex) {}

	std::vector<unsigned char> data() const { return m_data; }
	unsigned int odIndex() const { return m_odIndex;}

private:
	std::vector<unsigned char> m_data;
	unsigned int m_odIndex; /* Object dictionary index to which this transfer wrote to */
};


class SdoSystem
{
public:
	SdoSystem( boost::function<void(SdoReplyFrame)> replySender);
	virtual ~SdoSystem();

	void addObject ( std::shared_ptr<SdoObject> o) { o->setParent(this); m_objects.push_back(o); }

	bool handle ( const can_frame &f );


	/* Logging of segmented transfer data */
	bool logSegmented() const { return mSegmentedSdoStoreDownloaded; }
	void enableLoggingSegmented(const std::string & fileName);
	void logSegmentedTransfer( const CElmbSegmentedSdoTransferData& data );
	void writeStoredSegmentedTransfersToFile ();

private:
	std::list<std::shared_ptr<SdoObject>> m_objects;

	std::shared_ptr<SdoObject> findObject (unsigned int objectNumber, unsigned char subObjectNumber);

	bool handleExpeditedRead (const can_frame &f );
	bool handleExpeditedWrite (const can_frame &f );
	boost::function<void(SdoReplyFrame)> m_replySender;

	std::shared_ptr<SdoObject> m_lastObject;

	/* segmentedSDO download logging */

	bool mSegmentedSdoStoreDownloaded;
	std::string mSegmentedSdoStoreFileName;
	std::list < CElmbSegmentedSdoTransferData > mSegmentedSdoDownloadTransfers;
	typedef std::list < CElmbSegmentedSdoTransferData* >::iterator MSegmentedSdoDownloadTransfersIterator;

	void writeOneTransfer (ofstream &f, const CElmbSegmentedSdoTransferData &xfer);
	boost::mutex mSegmentedSdoMutex;
};

#endif /* CORESIMULATOR_SDOSYSTEM_H_ */
