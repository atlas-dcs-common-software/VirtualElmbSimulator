/*
 * CCanFrameSender.h
 *
 *  Created on: Jun 18, 2013
 *      Author: pnikiel
 */

#ifndef CCANFRAMESENDER_H_
#define CCANFRAMESENDER_H_

#include "simulated.h"

namespace SimulatedCan {

class CCanFrameSender {
public:
	virtual void sendFrame (can_frame &f, double delay) = 0;
};

} /* namespace SimulatedCan */
#endif /* CCANFRAMESENDER_H_ */
