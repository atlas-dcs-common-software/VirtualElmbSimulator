/*
 * TODO udpate this
 */

#ifndef SOCKCANSCAN_H_
#define SOCKCANSCAN_H_

#include <pthread.h>
#include <unistd.h>

#include <string>
using namespace std;
#include "CCanAccess.h"

#include <list>
#include <vector>

#include <time.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <unistd.h>
#include "simulated.h"
#include "CElmb.h"
#include "CCanFrameSender.h"
#include <iostream>
#include "CoreSimulator.hxx"

class CSimulatedCanScanInternal;

class CSimulatedCanScan : public CCanAccess, public SimulatedCan::CCanFrameSender
{
public:
	CSimulatedCanScan()  ;
	virtual ~CSimulatedCanScan();

    virtual bool sendMessage(short cobID, unsigned char len, unsigned char *message);
	virtual bool sendRemoteRequest(short cobID);
	virtual bool createBUS(const char *name ,const  char *parameters );
	void sendFrameFromElmb (can_frame &f, double delay);
	void yield ();
	virtual void sendFrame (can_frame &f, double delay);
	void dumpFramesLog ();
private:
	/* TODO Consider removing this ?? */
	bool run_can;
	void sendErrorMessage(const struct can_frame *);
	int configureCanboard(const char *,const char *);
	static void* CanScanControlThread(void *);

	void forwardCanFrameToStack (can_frame &f);
	std::string busName;
	friend class CSimulatedCanScanInternal;
	CSimulatedCanScanInternal *impl;
	::std::unique_ptr< ::Simulation > m_XmlConfigSimulation;
	bool mSimulateBadTimestamps;
	bool mRxFramesLog;
	bool mTxFramesLog;
	std::string mFramesLogFilename;
	unsigned int m_lastPortError;

};


#endif /* SOCKCANSCAN_H_ */
