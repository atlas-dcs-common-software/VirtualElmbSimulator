/*
 * CNetCommands.cpp
 *
 *  Created on: Nov 20, 2013
 *      Author: pnikiel
 */





#include "Poco/Net/TCPServer.h"
#include "Poco/Net/TCPServerConnection.h"
#include "Poco/Net/TCPServerConnectionFactory.h"
#include "Poco/Net/TCPServerParams.h"
#include "Poco/Net/StreamSocket.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTimeFormat.h"
#include "Poco/Exception.h"
#include "Poco/Util/ServerApplication.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/HelpFormatter.h"
#include <iostream>
#include <boost/thread/mutex.hpp>
#include <boost/tokenizer.hpp>
#include "CNetCommands.h"
#include "CUtils.h"
#include "CANopen.h"
#include <unistd.h>
#include <exception>
#include <stdio.h>
#include <stdlib.h>
#include <LogIt.h>

using Poco::Net::ServerSocket;
using Poco::Net::StreamSocket;
using Poco::Net::TCPServerConnection;
using Poco::Net::TCPServerConnectionFactory;
using Poco::Net::TCPServer;
using Poco::Timestamp;
using Poco::DateTimeFormatter;
using Poco::DateTimeFormat;
using Poco::Util::ServerApplication;
using Poco::Util::Application;



class TimeServerConnection: public TCPServerConnection
/// This class handles all client connections.
///
/// A string with the current date and time is sent back to the client.
{
public:
	TimeServerConnection(const StreamSocket& s,
			const std::vector <SimulatedCan::CCanBus*> *buses,
			const std::vector <CSimulatedCanScan*> *scans):
				TCPServerConnection(s),
				mBuses(buses),
				mCanScans(scans),
				mCharSeparator(" \n\r")
{
}



    void run()
    {

        while (1)
        {
            try
            {
                char rcv[1024];
                memset (rcv, 0, sizeof(rcv));
                if (socket().receiveBytes((void*)rcv,sizeof(rcv)-1, 0) == 0)
                {
                    // zero means graceful shutdown
                    return;
                }
                rcv[sizeof rcv - 1] = 0;
                std::cout << "rcvd:'" << rcv << "'" << std::endl;
                parseCommand(rcv);
            }
            catch (Poco::Exception& exc)
            {
                std::cout << "net cer" << std::endl;
            }

        }
    }

private:
	const std::vector <SimulatedCan::CCanBus*> *mBuses;
	const std::vector <CSimulatedCanScan*> *mCanScans;
	boost::char_separator<char> mCharSeparator;
	typedef boost::tokenizer<boost::char_separator<char> >
	tokenizer;

	void sendResponse (std::string s)
	{
		socket().sendBytes(s.c_str(), (int) s.length());
	}

	std::string tokenizeString (const tokenizer& tokens, tokenizer::iterator &it)
	{
		if (it == tokens.end())
			throw std::exception();
		return *it++;

	}

	int tokenizeInt (const tokenizer& tokens, tokenizer::iterator &it)
	{
		if (it == tokens.end())
			/* No more tokens! */
			throw std::exception ();
		int result =  atoi ((*it).c_str());
		it++;
		return result;
	}

	int tokenizeHex (const tokenizer& tokens, tokenizer::iterator &it, bool optional=false)
	{
		if (it == tokens.end())
		{
			/* No more tokens! */
			if (!optional)
				throw std::exception ();
			else
				return 0;


		}
		int result =  strtol ((*it).c_str(), (char**)0, 16);
		it++;
		return result;
	}

	SimulatedCan::CCanBus* getBus (std::string busName)
	{
		for (int i=0; i<mBuses->size(); i++)
		{
			if ((*mBuses)[i]->getName().compare(busName) == 0)
				return (*mBuses)[i];
		}
		throw std::runtime_error("No such bus: "+busName);
	}

    void parseCommand (const char* text)
    {
        std::string str (text);
        tokenizer tokens(str, mCharSeparator);
        tokenizer::iterator tok_iter = tokens.begin();
        try
        {
            if (tok_iter==tokens.end())
                return; /* Nothing to parse*/
            std::string cmd = *tok_iter++;
            std::cout << "cmd=" << cmd << "len=" << cmd.length()<< std::endl;
            if (cmd.compare("?")==0)
                sendResponse("no help\n");

            /* FIRST COMMANDS WHICH DO NOT TAKE BUS AS A FIRST ARG */
            if (cmd.compare("all_write_segmented_sdo_to_file")==0)
            {
                /* no params */
                /* get every registered CANbus */
                for (int i=0; i<=mBuses->size(); i++)
                {
                    const std::list<SimulatedCan::CElmb*> elmbs = (mBuses->at(i))->getListElmbs();
                    std::list<SimulatedCan::CElmb*>::const_iterator it;
                    for (it=elmbs.begin(); it!=elmbs.end(); it++)
                    {
                        (*it)->writeSegmentedSdoToFile();
                    }
                }
                sendResponse("DONE\n");
                return;
            }
            else if (cmd.compare("dump_frames_log")==0)
            {
                std::vector<CSimulatedCanScan*>::const_iterator it;
                it = mCanScans->begin();
                for (it=mCanScans->begin(); it!=mCanScans->end(); it++)
                {
                    (*it)->dumpFramesLog();
                }
                return;
            }
            else
            {
                // NOW Per-Bus commands                
                
                std::string bus_s = tokenizeString(tokens, tok_iter);
                std::list< SimulatedCan::CCanBus*> bus_list;
                if (bus_s == "*") // will apply the command to elmbs selected from all buses
                {
                    std::copy( mBuses->begin(), mBuses->end(), std::back_inserter(bus_list) );
                }
                else
                    bus_list.push_back( getBus(bus_s) );

                if (cmd.compare("set_bus_broken")==0)
                {
                    unsigned int broken = tokenizeInt(tokens, tok_iter);
                    for( auto bus: bus_list )
                    {
                    	bus->setBusBroken(broken != 0);
                    }
                    sendResponse("DONE\r\n");
                    return;

                    
                }


                
                std::list< SimulatedCan::CElmb* > elmb_list;
                std::string elmbId_s = tokenizeString(tokens, tok_iter);
                for (auto bus: bus_list)
                {
                    if (elmbId_s == "*")
                    {
                        auto elmbsOnBus = bus->getListElmbs();
                        std::copy( elmbsOnBus.begin(), elmbsOnBus.end(), std::back_inserter(elmb_list) );
                    }
                    else
                        elmb_list.push_back( bus->findElmb(atoi(elmbId_s.c_str())) );
                }


                // first commands which operatory solely on elmb (without specifying channels)
                if (cmd == "getnodenmtstate") /* params bus(string) node */
                {
                    for (auto elmb: elmb_list)
                        sendResponse( CANopen_getNmtStateText( elmb->getNmtState() )+std::string("\n") );
                    return;
                }
                else if (cmd.compare("sendbootup")==0)
                {
                    for (auto elmb: elmb_list)
                        elmb->sendBootup();
                    sendResponse("DONE\r\n");
                    return;
                }
                else if (cmd.compare("get_num_received_rtr_tpdo3")==0)
                {
                    for (auto elmb: elmb_list)
                    {
                        unsigned int numRtrs = elmb->getCountReceivedRtrTPDO3();
                        char resp[10];
                        snprintf(resp, sizeof resp, "%u", numRtrs);
                        sendResponse(resp);
                    }
                    return;

                }
                else if (cmd.compare("get_num_received_sync")==0)
                {
                    for (auto elmb: elmb_list)
                    {
                        unsigned int numRtrs = elmb->getCountReceivedSync();
                        char resp[10];
                        snprintf(resp, sizeof resp, "%u", numRtrs);
                        sendResponse(resp);
                    }
                    return;

                }
                else if (cmd.compare("get_num_received_nodeguard")==0)
                {
                    for (auto elmb: elmb_list)
                    {
                        unsigned int numRtrs = elmb->getCountReceivedNodeGuard();
                        char resp[10];
                        snprintf(resp, sizeof resp, "%u", numRtrs);
                        sendResponse(resp);
                    }
                    return;

                }
                else if (cmd.compare("send_emergency")==0)
                {
                    unsigned int errorCode = tokenizeHex(tokens, tok_iter);
                    unsigned int errorReg = tokenizeHex(tokens, tok_iter, /*optional*/ true);
                    unsigned int errorByte1 = tokenizeHex(tokens, tok_iter, /*optional*/ true);
                    unsigned int errorByte2 = tokenizeHex(tokens, tok_iter, /*optional*/ true);
                    unsigned int errorByte3 = tokenizeHex(tokens, tok_iter, /*optional*/ true);
                    for (auto elmb: elmb_list)
                        elmb->sendEmergencyObject(errorCode, errorReg, errorByte1, errorByte2, errorByte3);
                    sendResponse("DONE\r\n");
                    return;

                }
                else if (cmd.compare("set_can_connectivity")==0)
                {
                    unsigned int connectivity = tokenizeInt(tokens, tok_iter);
                    for (auto elmb: elmb_list)
                        elmb->setCanConnectivityEnabled(connectivity!=0);
                    sendResponse("DONE\r\n");
                    return;
                }
                else if (cmd.compare("write_segmented_sdo_to_file")==0)
                {
                    for (auto elmb: elmb_list)
                        elmb->writeSegmentedSdoToFile();
                    sendResponse("DONE\r\n");
                    return;
                }
                else if (cmd == "setanaloginput") /* params: bus node channel value */
                {
                    std::string channel_s = tokenizeString(tokens, tok_iter);
                    unsigned int value = tokenizeInt(tokens, tok_iter);

                    for (auto elmb: elmb_list)
                    {
                        if (channel_s == "*")
                            for (int channel_i=0; channel_i<64; channel_i++)
                                elmb->setAnalogInput(channel_i,value);
                        else
                        {
                            int channel_i = atoi (channel_s.c_str());
                            elmb->setAnalogInput(channel_i,value);
                        }
                    }

                    sendResponse("DONE\r\n");
                    return;
                }
                else if (cmd.compare("set_digital_input")==0)
                {
                    std::string port = tokenizeString(tokens, tok_iter);
                    unsigned int value = tokenizeInt(tokens, tok_iter);
                    for (auto elmb: elmb_list)
                        elmb->setDigitalInput(port, value);
                    sendResponse("DONE\r\n");
                    return;
                }
                else if (cmd.compare("get_digital_input")==0)
                {
                    std::string port = tokenizeString(tokens, tok_iter);
                    for (auto elmb: elmb_list)
                        sendResponse(SimulatedCan::CUtils::toString((unsigned int)elmb->getDigitalInput(port)));
                    sendResponse("\r\n");
                    return;
                }

                else if (cmd.compare("getarrayregister")==0)
                {
                    std::string registerName = tokenizeString(tokens, tok_iter);
                    unsigned int byteFrom = tokenizeInt(tokens, tok_iter);
                    unsigned int byteNum = tokenizeInt(tokens, tok_iter);
                    for (auto elmb: elmb_list)
                    {
                        unsigned char* buffer = new unsigned char [byteNum];
                        if (!buffer)
                            throw std::runtime_error("failed allocation buffer"); // failed allocating
                        elmb->getArrayRegister(registerName, byteFrom, byteNum, buffer);
                        /* now 'textualize' and send */
                        std::string response;
                        response.reserve(2*byteNum);
                        for (int i=0; i<byteNum; i++)
                        {
                            char letters[3];
                            snprintf(letters, sizeof(letters), "%02x", buffer[i]);
                            response += letters[0];
                            response += letters[1];
                        }
                        sendResponse(response);
                        delete[] buffer;
                    }
                    return;

                }
                else if (cmd.compare("write_test9999")==0)
                {
                    /* params: bus node contents */
                    std::string contents( tokenizeString(tokens, tok_iter) );
                    for (auto elmb: elmb_list)
                        elmb->setTestBuffer9999( contents );
                    sendResponse("DONE\r\n");
                    return;
                }
                else if (cmd.compare("read_test9999")==0) /* params: bus node  */
                {
                    for (auto elmb: elmb_list)
                        sendResponse( elmb->getTestBuffer9999() );
                    return;
                }
                else if (cmd.compare("set_conversion_flag")==0) /* params bus node channel flag */
                {
                    std::string channel_s = tokenizeString(tokens, tok_iter);
                    const unsigned char flag = tokenizeInt(tokens, tok_iter);
                    for (auto elmb: elmb_list)
                    {
                        if (channel_s == "*")
                            for (int channel_i=0; channel_i<64; channel_i++)
                                elmb->setConversionFlag( channel_i, flag);
                        else
                        {
                            int channel_i = atoi (channel_s.c_str());
                            elmb->setConversionFlag( channel_i, flag );
                        }
                    }
                    sendResponse( "DONE\n" );
                    return;
                }

                else
                    throw std::runtime_error("command invalid");
            }


        } catch (std::exception &e)
        {
            sendResponse (std::string("FAIL ")+e.what()+"\r\n");
        }
    }
};


class CNetCommandsConnectionFactory: public TCPServerConnectionFactory

{
public:
	CNetCommandsConnectionFactory(const std::vector <SimulatedCan::CCanBus*> *buses, const std::vector <CSimulatedCanScan*> *canScans):
		mBuses(buses),
		mCanScans(canScans)
{

}

	TCPServerConnection* createConnection(const StreamSocket& socket)
	{
		return new TimeServerConnection(socket, mBuses, mCanScans);
	}

private:
	const std::vector <SimulatedCan::CCanBus*> *mBuses;
	const std::vector <CSimulatedCanScan*> *mCanScans;

};





CNetCommands* CNetCommands::mInstance = 0;
boost::mutex CNetCommands::mMutex;

CNetCommands::CNetCommands()
{
	std::cout << "CNetCommands:welcome" << std::endl;
	unsigned short port = 9911;
	std::string format(DateTimeFormat::ISO8601_FORMAT);

	const char* portEnvVarName = "VIRTUAL_ELMB_SIM_PORT";
	const char* portFromEnv = getenv(portEnvVarName);
	if (portFromEnv)
	{
		char *endptr;
		port = strtol( portFromEnv, &endptr , 10 );
		if (endptr == portFromEnv || *endptr!='\0')
			throw std::runtime_error("Port number specified through environment variable "+std::string(portEnvVarName)+" has invalid format.");
		LOG(Log::WRN) << "Running on non-standard port: " << port;
	}

	try
	{
		// set-up a server socket
		ServerSocket svs(port);
		// set-up a TCPServer instance
		mServer = new TCPServer(new CNetCommandsConnectionFactory(&buses, &canScans), svs);
		// start the TCPServer
		mServer->start();
	}
	catch (Poco::Exception &e)
	{
		throw std::runtime_error("Exception while opening NetCommands port: "+e.message());
	}

}

CNetCommands::~CNetCommands()
{
	mServer->stop();
}

CNetCommands* CNetCommands::getInstance()
{
	mMutex.lock();
	if (mInstance == 0 ) {
		mInstance = new CNetCommands ();
	}
	mMutex.unlock();
	return mInstance;
}

void CNetCommands::registerBus(SimulatedCan::CCanBus *bus)
{
	std::cout << "Bus:" << bus->getName() << "registered" << std::endl;
	buses.push_back(bus);

}

void CNetCommands::registerCanScan(CSimulatedCanScan * canScan)
{
	canScans.push_back(canScan);
}
