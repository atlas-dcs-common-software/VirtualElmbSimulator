/*
 * CANopen.h
 *
 *  Created on: Nov 21, 2013
 *      Author: pnikiel
 */

#ifndef CANOPEN_H_
#define CANOPEN_H_


#define NMT_STATE_INITIALISING 0
#define NMT_STATE_STOPPED	   4
#define NMT_STATE_OPERATIONAL  5
#define NMT_STATE_PREOPERATIONAL 127

const char* CANopen_getNmtStateText (unsigned int nmt);

#endif /* CANOPEN_H_ */
