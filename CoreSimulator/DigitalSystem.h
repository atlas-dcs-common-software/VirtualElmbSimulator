/*
 * DigitalSystem.h
 *
 *  Created on: Oct 26, 2015
 *      Author: pnikiel
 */

#ifndef CORESIMULATOR_DIGITALSYSTEM_H_
#define CORESIMULATOR_DIGITALSYSTEM_H_

#include <vector>

enum PortId
{
	PORT_A=0,
	PORT_C,
	PORT_F,
	Count
};


class DigitalSystem
{
public:
	DigitalSystem();
	virtual ~DigitalSystem();

	// by byte 
	void setValue (PortId which, unsigned char val);
	unsigned char getValue (PortId which);
	// by bit
	void setValueByBit (PortId which, unsigned char bitNumber, bool value);
	bool getValueByBit (PortId which, unsigned char bitNumber);

	void setOutputMask (PortId which, unsigned char val) { m_outputMasks.at(which) = val; }
	unsigned char getOutputMask (PortId which) { return m_outputMasks.at(which); }

	void setInterruptMask (PortId which, unsigned char val) { m_interruptMasks.at(which) = val; }
	unsigned char getInterruptMask (PortId which) { return m_interruptMasks.at(which); }

private:
	//! Most up-to-date state of given digital port
	//! TODO: change into std::vector
	unsigned char m_values [PortId::Count];

	//! Most up-to-date state of given ports' output mask (SDO object 6208/xx)
	std::vector<unsigned char> m_outputMasks;

	//! Most up-to-date state of given ports' interrupt mask (SDO object 6006/xx)
	std::vector<unsigned char> m_interruptMasks;

};

#endif /* CORESIMULATOR_DIGITALSYSTEM_H_ */
