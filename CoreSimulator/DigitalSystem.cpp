/*
 * DigitalSystem.cpp
 *
 *  Created on: Oct 26, 2015
 *      Author: pnikiel
 */

#include "DigitalSystem.h"
#include <stdexcept>

DigitalSystem::DigitalSystem() :
    m_outputMasks( PortId::Count, 0 ),
    m_interruptMasks( PortId::Count, 0)
{
	// TODO Auto-generated constructor stub

}

DigitalSystem::~DigitalSystem()
{
	// TODO Auto-generated destructor stub
}

void DigitalSystem::setValue(PortId which, unsigned char val)
{
	if (which >= PortId::Count)
		throw std::logic_error("PortId out of bounds");
	m_values[which] = val;
}

unsigned char DigitalSystem::getValue(PortId which)
{
    if (which >= PortId::Count)
	throw std::logic_error("PortId out of bounds");
    return m_values[which];
}

void DigitalSystem::setValueByBit (PortId which, unsigned char bitNumber, bool value)
{
    if (which >= PortId::Count || bitNumber >7)
	throw std::logic_error("PortId out of bounds or bitNumber >7");
    m_values[which] = value ? (m_values[which] | (1 << bitNumber)) : (m_values[which] & ~(1 << bitNumber));
}

bool DigitalSystem::getValueByBit (PortId which, unsigned char bitNumber)
{
    if (which >= PortId::Count || bitNumber >7)
	throw std::logic_error("PortId out of bounds or bitNumber >7");
    return m_values[which] & (1 << bitNumber);
}
