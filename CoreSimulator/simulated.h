/*
 * simulated.h
 *
 *  Created on: Jun 17, 2013
 *      Author: pnikiel
 */

#ifndef SIMULATED_H_
#define SIMULATED_H_

extern bool printDebugs;


#include <sys/socket.h>
#include <linux/can.h>

#define SIMULATED_RTR_FLAG 0x8000

#endif /* SIMULATED_H_ */
