/*
 * SdoObject.h
 *
 *  Created on: Oct 27, 2015
 *      Author: pnikiel
 */

#ifndef CORESIMULATOR_SDOOBJECT_H_
#define CORESIMULATOR_SDOOBJECT_H_

#include <boost/optional.hpp>

// TODO: for can_frame
#include <sys/socket.h>
#include <linux/can.h> // for can_frame

class SdoSystem;

//! Rationale: SDO system reply transmission params may depend.
//! I.e. the following was made to be able to send segmented sdo replies with different delays than non segmented sdos
class SdoReplyFrame
{
public:
	SdoReplyFrame( const can_frame& f, bool isSegmented ):
		m_frame(f),
		m_isSegmented(isSegmented) {}
	can_frame frame() const { return m_frame; }
	bool isSegmented() const { return m_isSegmented;}
private:
	can_frame m_frame;
	bool m_isSegmented;
};

class SdoObject
{
public:

	SdoObject( unsigned int objectNumber, unsigned char subObjectNumber );
	//! This constructor will match any subobject of this object
	SdoObject( unsigned int objectNumber );
	virtual ~SdoObject();

	virtual boost::optional<SdoReplyFrame> initiateDomainDownload (const can_frame &f ) = 0;
	virtual boost::optional<SdoReplyFrame> initiateDomainUpload (const can_frame &f) = 0;
	virtual boost::optional<SdoReplyFrame> downloadDomainSegment (const can_frame &f) = 0;
	virtual boost::optional<SdoReplyFrame> uploadDomainSegment (const can_frame &f) = 0;

	//! True if and only if given (object, subobject) matches this object
	bool matchesIds (unsigned int object, unsigned char subObject);

	void setParent( SdoSystem *parent ) { m_parent = parent; }

protected:
	const unsigned int m_objectNumber;
	const unsigned char m_subObjectNumber;
	const bool m_anySubObject;
	SdoSystem * m_parent;
	SdoSystem * parent() { return m_parent; }

};

#endif /* CORESIMULATOR_SDOOBJECT_H_ */
