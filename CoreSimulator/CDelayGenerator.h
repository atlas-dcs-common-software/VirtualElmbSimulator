/*
 * CDelayGenerator.h
 *
 *  Created on: Dec 13, 2013
 *      Author: dcs
 */

#ifndef CDELAYGENERATOR_H_
#define CDELAYGENERATOR_H_

#include <string>

namespace SimulatedCan {

class CDelayGenerator {
public:
	CDelayGenerator(const std::string &params);
	virtual ~CDelayGenerator();

	double getDelay() const;

private:
	enum {DELAY_CONST, DELAY_UNIFORM} delayType;
	double lower,upper;

};

} /* namespace SimulatedCan */

#endif /* CDELAYGENERATOR_H_ */
