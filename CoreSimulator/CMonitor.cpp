/*
 * CMonitor.cpp
 *
 *  Created on: Jun 18, 2013
 *      Author: pnikiel
 */

#include <stdio.h>
#include <iostream>
#include "CMonitor.h"
#include "CUtils.h"

namespace SimulatedCan {


CMonitor* CMonitor::getInstance ()
{
	mMutex.lock();
	if (mInstance == 0 ) {
		mInstance = new CMonitor ();
	}
	mMutex.unlock();
	return mInstance;
}

CMonitor::CMonitor(): handles(0), lastSymbol('/'), redrawInterval(0.1)
{
	std::cout << "CMonitor ctr this=" << this << std::endl;
}

int CMonitor::obtainHandle() {
	int result;
	mMutex.lock ();
	result = handles;
	handles++;
	infos.push_back(CMonitorInfo());
	mMutex.unlock ();
	return result;
}

void CMonitor::setBusName (int handle, std::string busName)
{
	mMutex.lock ();
	if (handle >= handles)
	{
		mMutex.unlock();
		return; /* given handle is invalid */
	}
	infos[handle].mBusName.assign (busName);
	mMutex.unlock ();
}

void CMonitor::updateInfo (int handle, int yield, int channelYield) {
	mMutex.lock ();
	if (handle >= handles)
		return; /* given handle is invalid */
	infos[handle].yieldRate = yield;
	infos[handle].channelYieldRate = channelYield;
	/* TODO see if time passed to redraw may be needed */
	timeval tnow;
	gettimeofday (&tnow, 0);
	if (CUtils::getTimeDiff(&lastRedraw, &tnow) >= redrawInterval)
	{
		redraw ();
		lastRedraw = tnow;
	}
	mMutex.unlock ();
}

void CMonitor::redraw ()
{

	switch (lastSymbol) {
		case '/': lastSymbol='-'; break;
		case '-': lastSymbol='\\'; break;
		case '\\': lastSymbol='|'; break;
		case '|': lastSymbol='/'; break;
	}

	std::cout << "\r" << lastSymbol << ' ';
	int yieldRateSum=0, channelYieldRateSum=0;
	for (int i=0; i<handles; i++) {
		printf ("%s %6.0lf %6.0lf | ",infos[i].mBusName.c_str(), (double)infos[i].yieldRate/redrawInterval,(double)infos[i].channelYieldRate/redrawInterval);
		yieldRateSum += infos[i].yieldRate;
		channelYieldRateSum += infos[i].channelYieldRate;
	}
	printf ("TOTAL(SERVER) %6.0lf %6.0lf        ",(double)yieldRateSum / redrawInterval,(double)channelYieldRateSum / redrawInterval);
	fflush(stdout);

}


CMonitor* CMonitor::mInstance = 0;
boost::mutex CMonitor::mMutex;

} /* namespace SimulatedCan */
