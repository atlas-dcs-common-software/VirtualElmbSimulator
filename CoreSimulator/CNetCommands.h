/*
 * CNetCommands.h
 *
 *  Created on: Nov 20, 2013
 *      Author: dcs
 */

#ifndef CNETCOMMANDS_H_
#define CNETCOMMANDS_H_

#include "Poco/Net/TCPServer.h"
#include "CCanBus.h"
#include "SimulatedCanScan.h"
#include <vector>


class CNetCommands {
public:
	static CNetCommands *getInstance();
	void registerBus (SimulatedCan::CCanBus * bus);
	void registerCanScan (CSimulatedCanScan *canScan);
private:
	CNetCommands();
	~CNetCommands();
	static CNetCommands *mInstance;
	static boost::mutex mMutex;
	Poco::Net::TCPServer *mServer;
	std::vector<SimulatedCan::CCanBus*> buses;
	std::vector<CSimulatedCanScan*> canScans;
};


#endif /* CNETCOMMANDS_H_ */
