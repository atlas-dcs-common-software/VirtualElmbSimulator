/*
 * HandledSdoObject.cpp
 *
 *  Created on: Oct 27, 2015
 *      Author: pnikiel
 */

#include "HandledSdoObject.h"
#include <LogIt.h>

#include "SdoSystem.h"

HandledSdoObject::~HandledSdoObject()
{
	// TODO Auto-generated destructor stub
}

HandledSdoObject::HandledSdoObject(
		unsigned int objectNumber,
		unsigned char subObjectNumber,
		boost::function<SdoData()> readHandler,
		boost::function<void(SdoData)> writeHandler):
			SdoObject( objectNumber, subObjectNumber ),
			m_readHandler(readHandler),
			m_writeHandler(writeHandler),
			m_isSegmentedOngoing(false)
{
}

HandledSdoObject::HandledSdoObject(unsigned int objectNumber,
		boost::function<SdoData()> readHandler,
		boost::function<void(SdoData)> writeHandler):
					SdoObject( objectNumber),
					m_readHandler(readHandler),
					m_writeHandler(writeHandler),
					m_isSegmentedOngoing(false)
{

}

boost::optional<SdoReplyFrame> HandledSdoObject::initiateDomainDownload(
		const can_frame& f)
{
	bool expedited = (f.data[0] & 0x02) != 0;
	if (expedited)
	{
		m_isSegmentedOngoing = false;
		SdoData data (&f.data[4], 4);
		try
		{
			if (write (data))
			{
				can_frame respFrame;
				respFrame.can_id = 0x580 + (f.can_id & 0x7f);
				respFrame.can_dlc = 8;
				respFrame.data[0] = 0x60;
				respFrame.data[1] = f.data[1];
				respFrame.data[2] = f.data[2];
				respFrame.data[3] = f.data[3];
				return boost::optional<SdoReplyFrame>(SdoReplyFrame(respFrame, /*segmented*/ false));
			}
			else
			{
				return boost::optional<SdoReplyFrame> ();
			}
		}
		catch (NoSuchExpeditedSdoObject &)
		{
			return boost::optional<SdoReplyFrame> ();
		}
	}
	else
	{
		LOG(Log::INF) << "Will start segmented download";
		// segmented
		can_frame respFrame;
		respFrame.can_id = 0x580 + (f.can_id & 0x7f);
		respFrame.can_dlc = 8;
		respFrame.data[0] = 0x60;
		respFrame.data[1] = f.data[1];
		respFrame.data[2] = f.data[2];
		respFrame.data[3] = f.data[3];
		m_isSegmentedOngoing = true;
		m_segmentedDataBuffer.clear();
		m_done = 0;
		return boost::optional<SdoReplyFrame>(SdoReplyFrame(respFrame, /*segmented*/true ));
	}

}

boost::optional<SdoReplyFrame> HandledSdoObject::initiateDomainUpload(
		const can_frame& f)
{
	SdoData d = read();
	if (d.length() > 0)
	{
		if (d.length() > 4)
		{
			// requires segmented transfer
			if (m_readHandler.empty()) // nobody to provide the factual data
				return boost::optional<SdoReplyFrame>();
			m_isSegmentedOngoing = true;
			m_segmentedDataBuffer = d.asVector();
			m_done = 0;
			mSdoUploadNextToggleBitTx = false;
			unsigned int mSdoUploadBufferSize = d.length();
			LOG(Log::TRC) << "Beginning segmented SDO transfer with length=" << mSdoUploadBufferSize;
			can_frame respFrame;
			//TODO -> std::copy
			memset ((char*)&respFrame, 0, sizeof respFrame);
			respFrame.can_id = 0x580 + (f.can_id & 0x7f);
			respFrame.can_dlc = 8;
			/* TODO this is ugly, redo it */
			respFrame.data[0] = 0x41; // segmented  +  contain byte counter
			std::copy(
				&f.data[1],
				&f.data[4],
				&respFrame.data[1]
			);
			respFrame.data[4] = (unsigned int)mSdoUploadBufferSize & 0xff;
			respFrame.data[5] = (((unsigned int)mSdoUploadBufferSize) & 0xff00) >> 8;
			return boost::optional<SdoReplyFrame>(SdoReplyFrame(respFrame, /*segmented*/ true));
		}
		else
		{
			LOG(Log::TRC) << "Reply to SDO initiate domain upload, expedited, size=" << d.length();
			can_frame respFrame;
			respFrame.can_id = 0x580 + (f.can_id & 0x7f);
			respFrame.can_dlc = 8;
			/* TODO this is ugly, redo it */
			respFrame.data[0] = 0x43 | (((4-d.length()) &0x03)<<2);
			for (int i=1; i<=3; i++)
				respFrame.data[i] = f.data[i];
			for (unsigned int i=0; i<d.length(); i++)
				respFrame.data[4+i] = d.data[i];
			return boost::optional<SdoReplyFrame> (SdoReplyFrame(respFrame, /*segmented*/ false));
		}
	}
	else
		return boost::optional<SdoReplyFrame> ();
}

boost::optional<SdoReplyFrame> HandledSdoObject::downloadDomainSegment(
		const can_frame& f)
{
	LOG(Log::TRC) << "+" << __PRETTY_FUNCTION__;
	if (!m_isSegmentedOngoing)
		return boost::optional<SdoReplyFrame> ();

	bool moreSegments = (f.data[0] & 0x01) == 0;
	bool toggle = (f.data[0] & 0x10) != 0;
	/* stores number of data bytes in the frame */
	int numBytes = 7-((f.data[0] & 0x0e) >> 1);
	m_segmentedDataBuffer.insert( m_segmentedDataBuffer.end(), f.data+1, f.data+1+numBytes );
	m_done += numBytes;
	if (!moreSegments)
	{
		LOG(Log::INF) << "segmented download finished (" << m_done << " bytes)";
		SdoData data (m_segmentedDataBuffer);
		write (data); // notifies the receiver and passes them received data
		if (parent()->logSegmented())
		{
			CElmbSegmentedSdoTransferData xfer (m_objectNumber, data.asVector());
			parent()->logSegmentedTransfer(xfer);
		}
		m_isSegmentedOngoing = false;
	}

	if (toggle == mSdoLastToggleBitRx)
		LOG(Log::ERR) << "Toggle bit failure in downloading domain segment";

	can_frame respFrame;
	respFrame.can_id = 0x580 + (f.can_id & 0x7f);
	respFrame.can_dlc = 8;

	respFrame.data[0] = 0x20;
	if (mSdoNextToggleBitTx)
		respFrame.data[0] |= 0x10;

	mSdoLastToggleBitRx = toggle;
	mSdoNextToggleBitTx = !mSdoNextToggleBitTx;
	return boost::optional<SdoReplyFrame>(SdoReplyFrame(respFrame, /*segmented*/true) );
}

boost::optional<SdoReplyFrame> HandledSdoObject::uploadDomainSegment(
		const can_frame& f)
{
	can_frame respFrame;
	memset ((char*)&respFrame, 0, sizeof(respFrame));
	respFrame.can_id = 0x580 + (f.can_id & 0x7f);
	respFrame.can_dlc = 8;

	/* Ok send next segment */
	unsigned int left = m_segmentedDataBuffer.size() - m_done;
	unsigned int thisPart=left;
	if (thisPart > 7)
	{
		thisPart = 7;
	}
	else
	{
		/* Mark 'last segment' field */
		respFrame.data[0] |= 0x01;
	}
	/* Mark number of bytes send */
	respFrame.data[0] |= (7-thisPart) << 1;
	if (mSdoUploadNextToggleBitTx)
		respFrame.data[0] |= 0x10; //mark toggle
	mSdoUploadNextToggleBitTx = !mSdoUploadNextToggleBitTx;
	/* Copy data to data bytes */
	std::copy( m_segmentedDataBuffer.begin()+m_done, m_segmentedDataBuffer.begin()+m_done+thisPart, respFrame.data+1);
	m_done += thisPart;
	return boost::optional<SdoReplyFrame>(SdoReplyFrame(respFrame, /*segmented*/ true) );
}

SdoData HandledSdoObject::read()
{
	if (m_readHandler.empty())
		return SdoData ();
	else
		return m_readHandler ();
}

bool HandledSdoObject::write(SdoData data)
{
	if (m_writeHandler.empty())
		return false;
	else
	{
		m_writeHandler ( data );
		return true;
	}

}
