/*
 * CElmbFrameFactory.cpp
 *
 *  Created on: Jun 17, 2013
 *      Author: pnikiel
 */

#include "CElmbFrameFactory.h"
#include "simulated.h"
#include "iostream"

namespace SimulatedCan {

void CElmbFrameFactory::makeNmtSlaveNodeGuard (can_frame *f, unsigned int node, bool toggle, unsigned int state)
{
	if (printDebugs)
		std::cout << "makeNmtSlaveNodeGuard..." << std::endl;
	f->can_id = 0x700 + node;
	f->can_dlc = 1;
	f->data[0] = (toggle?0x80:0) | (state & 0x07f);
}

void CElmbFrameFactory::makeTpdo1 (can_frame *f, unsigned int node, unsigned char portA, unsigned char portC, unsigned char portF)
{
	if (printDebugs)
		std::cout << "CElmbSimCanFrameMaker::makeTpdo1..." << std::endl;
	f->can_id = 0x180 + node;
	f->can_dlc = 3;
	f->data[0] = portF; 
	f->data[1] = portA; 
	f->data[2] = portC; 
}

void CElmbFrameFactory::makeTpdo3 (can_frame *f, unsigned int node, unsigned int channel, unsigned int adcValueUv, unsigned char flag)
{
	if (printDebugs)
		std::cout << "CElmbSimCanFrameMaker::makeTpdo3..." << std::endl;
	f->can_id = 0x380 + node;
	f->can_dlc = 6;
	f->data[0] = channel;
	f->data[1] = flag;
	f->data[2] = adcValueUv & 0xff;
	f->data[3] = (adcValueUv & 0xff00) >> 8;
	f->data[4] = (adcValueUv & 0xff0000) >> 16;
	f->data[5] = (adcValueUv & 0xff000000) >> 24;

}

void CElmbFrameFactory::makeBootup(can_frame *f, unsigned int node)
{
	f->can_id = 0x700 + node;
	f->can_dlc = 1;
	f->data[0] = 0;
}

} /* namespace SimulatedCan */
