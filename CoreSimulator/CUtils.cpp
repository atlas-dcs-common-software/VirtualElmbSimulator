/*
 * CUtils.cpp

 *
 *  Created on: Jun 18, 2013
 *      Author: pnikiel
 */
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include "CUtils.h"
#include "simulated.h"


namespace SimulatedCan {


double CUtils::getTimeDiff (timeval *t0, timeval *t1)
{
	time_t diffSec = t1->tv_sec - t0->tv_sec;
	suseconds_t diffUsec = t1->tv_usec - t0->tv_usec;

	diffUsec += diffSec*1000000;
	return (double)diffUsec / 1.0E6;
}

void CUtils::delaySeconds (float s)
{
#define USE_NANOSLEEP

#ifdef USE_NANOSLEEP
	timespec t;
	unsigned long ns = (unsigned long)(1.0E9*s);
	t.tv_sec = ns / 1000000000;
	t.tv_nsec = ns % 1000000000;
	if (nanosleep (&t, 0) != 0)
		perror ("nanosleep");
#else
	usleep ((int)(1.0E6*s));
#endif

}

void CUtils::printCanFrame (const can_frame &f, bool force)
{
	if (printDebugs || force)
	{
		std::cout << "can_frame[id=0x"<<std::hex<<f.can_id<<" len="<<(unsigned int)f.can_dlc<<" data=";
		for (int i=0; i<f.can_dlc; i++)
			std::cout << (unsigned int)f.data[i] << " ";
		std::cout << std::endl;
	}
}

std::string CUtils::canFrameToString(const can_frame &f)
{
	std::ostringstream oss;
	oss<<"id=";
	oss.width(6);
	oss<<std::hex<<f.can_id<<" len="<<(unsigned int)f.can_dlc<<" ";
	for (int i=0; i<f.can_dlc; i++)
		oss << (unsigned int)f.data[i] << " ";
	return oss.str();
}
bool CUtils::checkInterval (timeval *lastTime, double interval)
{
	timeval tnow;
	gettimeofday (&tnow, 0);
	double diff = CUtils::getTimeDiff (lastTime, &tnow);
	if (diff < 0)
	{
		std::cerr << "WARNING: checkInterval: negative time interval -- clockskew??" << std::endl;
	}
	if (diff >= interval)
	{
		*lastTime = tnow;
		return true;
	}
	else
		return false;
}
//TODO: remapCanPort should be actually a composition of cuntion portNameToPortNumber and portNumberToPortName !!!!
/* e.g. remapCanPort ('can0', out, 3) should put out='can3' */
void CUtils::remapCanPort (const std::string& in, std::string& out)
{
	if (portShift==0)
	{
		out.assign(in);
		return;
	}
	if (in.length() < 4)
		throw std::exception();
	/* In format -- does it begin with can? */
	if ( in.substr(0,3).compare("can") != 0)
		throw std::runtime_error("remap requested, but busname doesnt start with 'can'" );
	std::string num_s = in.substr(3);
    int num = atoi (num_s.c_str());
    out.assign("can");
    out.append(toString(num+portShift));
}

int CUtils::portShift = 0;

void CUtils::byteToHex (unsigned char c, std::string &out)
{
	char buf[4];
	snprintf(buf, sizeof(buf), "%02X", c);
	out.assign(buf);
}

unsigned char CUtils::hexToByte( std::string in )
{
	unsigned int x;
	std::stringstream ss;
	ss << std::hex << in;
	ss >> x;
	return x;
}

} /* namespace SimulatedCan */
