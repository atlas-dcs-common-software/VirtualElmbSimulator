/*
 * SockCanScan.cpp
 *
 *  Created on: Jul 21, 2011
 *      Author: vfilimon
 */
#include "SimulatedCanScan.h"
#include <errno.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction

#include <iostream>
#include <fstream>

#include <time.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <unistd.h>


#include "CElmb.h"
#include "CCanBus.h"
#include "CMonitor.h"
#include "CUtils.h"
#include "CoreSimulator.hxx"
#include "CNetCommands.h"

#include <LogIt.h>
#include <iomanip>

bool printDebugs = true;


// the delayed SDOs are not nicely designed
// because it is just  a dirty hack to test one important case

/* This is to model delayed answers from ELMBs.
 * The structure holds a can_frame with a proper response,
 * plus a time stamp of origination and requested delay */
class can_frame_with_delay
{
public:
	can_frame_with_delay (const can_frame &f, double delay)
{
	_f = f;
	_requestedDelay = delay;
	gettimeofday (&_originTime, 0);
}
	can_frame 	_f;
	timeval 	_originTime;
	double 		_requestedDelay;
}; 

class CCanFrameLogEntry
{
public:
	typedef enum {TX, RX} Direction;
	CCanFrameLogEntry (Direction d, const can_frame &f): m_Frame(f), m_Direction(d)
{
		gettimeofday (&m_Time, 0);
}
	std::string toString ()
	{
		std::string out;
		if (m_Direction == TX)
			out.assign("TX ");
		else
			out.assign("RX ");
		out.append (SimulatedCan::CUtils::canFrameToString(m_Frame));
		return out;
	}

	timeval getTime () {return m_Time;}
private:
	can_frame m_Frame;
	timeval m_Time;
	Direction m_Direction;
};

class CSimulatedCanScanInternal
{
    public:
    SimulatedCan::CMonitor *mMonitor;
    int mMonitorHandle;
    SimulatedCan::CCanBus mCanBus;

    std::vector<SimulatedCan::CElmb*> elmbs;
    list<can_frame> m_framesToElmbs;
    list<can_frame_with_delay> m_framesFromElmbs;
    list<CCanFrameLogEntry> m_FramesLog;
    pthread_t   m_hCanScanThread;
    int         m_idCanScanThread;
    boost::mutex mLock;
    CSimulatedCanScan *mParent;
    CNetCommands *mNetCommands;


    CSimulatedCanScanInternal ()

    {

    }



    void yield ()
    {

        std::vector<SimulatedCan::CElmb*>::iterator it;
        for (it=elmbs.begin(); it!=elmbs.end(); it++)
            {
                (*it)->yield();
            }
    }

    void sendFrameFromElmb (can_frame &f, double delay)
    {
        if (mCanBus.getBusBroken())
            return;  //  completely ignore messages from ELMB
        if (printDebugs)
            cout << "Scheduling frame to be sent with delay=" << delay << endl;
        can_frame_with_delay fd (f, delay);
        mLock.lock();
        m_framesFromElmbs.push_back(fd);

        int numEl = m_framesFromElmbs.size();
        mLock.unlock();
        if (numEl>1000)
            {
                std::cout << "Warning: queue of frames from ELMB is now " << numEl << " messages long" << std::endl;
            }
    }
    void forwardFromServerToElmbs ()
    {
        mLock.lock();
        /* If anything buffered, forward frames from OPC Server To Elmbs */
        if (m_framesToElmbs.size() > 0)
            {
                can_frame f = m_framesToElmbs.front ();
                m_framesToElmbs.pop_front ();
                mLock.unlock ();
                mCanBus.processFrame(f);
            }
        else
            mLock.unlock();
    }

    void forwardFromElmbsToServer ()
    {
        /* Either forward frames from Elmbs to OPC Server (if anything is buffered) or generate frames with measurements */
        mLock.lock();
        int maxToPass=10;
        int inQueue = m_framesFromElmbs.size();
        int togo = inQueue;
        /* Iterate over all ELMBs and look for frames whose delay already passed */
        list<can_frame_with_delay>::iterator it;
        for (it = m_framesFromElmbs.begin(); it != m_framesFromElmbs.end(); it++)
            {
                can_frame_with_delay cfwd = *it;
                if (SimulatedCan::CUtils::checkInterval (&cfwd._originTime, cfwd._requestedDelay))
                    {
                        if (printDebugs)
                            cout << "INFO: flushing frame that was scheduled with requested delay=" << cfwd._requestedDelay << " sec " << endl;
                        can_frame sockmsg = cfwd._f;
                        it = m_framesFromElmbs.erase (it);
                        /* We have to unlock, because only this way we can be sure, that no deadlock will occur
                         * but this is safe since we made a copy of frame to send (and not a pointer)*/
                        mLock.unlock();
                        mParent->forwardCanFrameToStack (sockmsg);
                        mLock.lock();
                    }
            }
        mLock.unlock();
    }
};
extern "C" CCanAccess *getCanbusAccess() __attribute__ ((visibility ("default")));

extern "C" CCanAccess *getCanbusAccess()
{
    CCanAccess *cc;
    cc = new CSimulatedCanScan();
    return cc;
}

CSimulatedCanScan::CSimulatedCanScan (): 
    impl(new CSimulatedCanScanInternal),
    mRxFramesLog(false),
    mTxFramesLog(false),
    mSimulateBadTimestamps(false),
	m_lastPortError(0)
{
    Log::initializeLogging(Log::TRC);
    LOG(Log::INF) << "Starting logging in INF";
    impl->mParent = this;
    try
	{
            CNetCommands::getInstance() -> registerCanScan(this);
	}
    catch (std::exception &e )
	{
            LOG(Log::ERR) << "Exception thrown from NetCommands: " << e.what();
            throw e;
	}
}

void CSimulatedCanScan::yield () { impl->yield(); }

void CSimulatedCanScan::forwardCanFrameToStack (can_frame &f)
{

	if (mTxFramesLog)
	{
		impl->m_FramesLog.push_back(CCanFrameLogEntry(CCanFrameLogEntry::TX, f));
	}
	
   	canMessage cmsg;
	cmsg.c_id = f.can_id;
	cmsg.c_dlc = f.can_dlc;
	memcpy(&cmsg.c_data[0],&f.data[0],8);
	if (mSimulateBadTimestamps)
	{
		cmsg.c_time.tv_sec = rand();
		cmsg.c_time.tv_usec = rand();
	}
	else
		gettimeofday (&cmsg.c_time,0);
	canMessageCame(cmsg);
	
}

void CSimulatedCanScan::sendFrame (can_frame &f, double d)
{
	impl->sendFrameFromElmb(f, d);
}

void* CSimulatedCanScan::CanScanControlThread(void *pCanScan)
{
	CSimulatedCanScan *ppcs = reinterpret_cast<CSimulatedCanScan *>(pCanScan);
	ppcs->run_can = true;
	usleep (1000000); /* On purpose, not to do resource starving from beginning */


	timeval tnow;
	timeval tyield, tconfig;

	int yieldCtr = 0;



	gettimeofday (&tnow, 0);	
	tyield = tconfig = tnow;
	while (ppcs->run_can) {
		gettimeofday (&tnow, 0);

		SimulatedCan::CUtils::delaySeconds (1.0E-4);
		if (ppcs->m_XmlConfigSimulation->showMonitor() && SimulatedCan::CUtils::getTimeDiff (&tyield, &tnow) >= 0.1) /* Time to display some status */
		{
			//	ppcs->lock ();
			ppcs->impl->mMonitor->updateInfo(ppcs->impl->mMonitorHandle, yieldCtr, 1);
			yieldCtr = 0;
			tyield = tnow;
			//	ppcs->unlock ();
		}
		ppcs->impl->forwardFromServerToElmbs();
		ppcs->impl->forwardFromElmbsToServer();
		ppcs->yield ();

	}

	pthread_exit(NULL);
	return NULL;
}

CSimulatedCanScan::~CSimulatedCanScan()
{
	run_can = false;
	delete impl;
}

int CSimulatedCanScan::configureCanboard(const char *name,const char *parameters)
{
	return 0;
}


bool CSimulatedCanScan::sendMessage(short cobID, unsigned char len, unsigned char *message)
{
    if (impl->mCanBus.getBusBroken())
        {
            // instead just an error frame
            timeval tv;
            gettimeofday( &tv, 0 );
            canMessageError( 4, "Timeout", tv);
            m_lastPortError = 1;
            return false;
            
            
        }
    else
    	if (m_lastPortError)
    	{
    		m_lastPortError = 0;
            timeval tv;
            gettimeofday( &tv, 0 );
            canMessageError( 0, "Port recovered", tv);
    	}
    
    
    struct can_frame frame;
    unsigned int nbytes;
    unsigned char *buf = message;
    frame.can_id = cobID;
    frame.can_dlc = len;
    int l, l1;

    l = len;
    do {
        if (l > 8) {
            l1 = 8; l = l - 8;
        }
        else l1 = l;
        frame.can_dlc = l1;
        memcpy(frame.data,buf,l1);

        if (printDebugs)
            {
                cout << "** Server sends ";
                SimulatedCan::CUtils::printCanFrame (frame);
            }

        impl->mLock.lock();

        if (mRxFramesLog)
            impl->m_FramesLog.push_back(CCanFrameLogEntry(CCanFrameLogEntry::RX, frame));
        impl->m_framesToElmbs.push_back (frame);

        impl->mLock.unlock();

        buf = buf + l1;
    }
    while (l > 8);
    return true; //sendErrorCode(Status);
}

bool CSimulatedCanScan::sendRemoteRequest(short cobID)
{
	
	unsigned char fake[8]={0};
	sendMessage (SIMULATED_RTR_FLAG |  cobID, 0, fake);
	/* TODO: it is not known whether this is safe to send out all RTR as empty frames */
	if (printDebugs)
		cout << "Warning: emitting RTR" << endl;
	return false;

}

bool CSimulatedCanScan::createBUS(const char *name ,const char *parameters)
{
	char cwd[200]="";
	getcwd (cwd, sizeof(cwd)-1);
	cout << "Hint: requested bus='" << name << "' current directory='" << cwd  <<"'" << endl;
	const char* usedName = name;
	// if colon is present in the name (so called long name), only the after-colon path shall be used
	const char* colon;
	if ((colon = strchr(name,':')) != 0)
		usedName = colon+1;
	/* Try to open simulation config */
	try
	{
		m_XmlConfigSimulation = simulation(std::string("sim_config.xml"));
	}
	catch (const xml_schema::exception& e)
	{
		cerr << e << endl;
		return false;
	}
	/* Find a bus that matches given name */
	// TODO probably could be done in more elegant way using predicates ...
	if (m_XmlConfigSimulation->debugLevel() > 0)
		printDebugs = true;
	else
		printDebugs = false;

	::Bus const *bus = 0;
	for (::Simulation::bus_const_iterator it=m_XmlConfigSimulation->bus().begin(); it!=m_XmlConfigSimulation->bus().end(); it++)
	{
		// *it is of type Bus
		if ((*it).portName().compare(usedName) == 0) /* strings equal */
		{
			bus = &(*it);
			break;
		}
	}
	if (!bus)
	{
		cerr << "Requested bus " << usedName << " is not present in sim_config.xml" << endl;

		return false;
	}


	run_can = true;
	impl->mCanBus.assignName(usedName);

	mSimulateBadTimestamps=bus->simulateBadTimestamps();
	mFramesLogFilename.assign(bus->framesDumpFileName());
	if (mFramesLogFilename!="")
	{
		mRxFramesLog=true;
		mTxFramesLog=true;
	}

	busName.assign (usedName);
	cout << "busName is assigned to " << busName << endl;

	impl->mMonitor = SimulatedCan::CMonitor::getInstance();
	std::cout << "CMonitor: using instance at " << impl->mMonitor << std::endl;
	impl->mMonitorHandle = impl->mMonitor->obtainHandle();
	impl->mMonitor->setBusName(impl->mMonitorHandle, usedName);

	impl->mNetCommands = CNetCommands::getInstance();
	std::cout << "CNetCommands: using instance at " << impl->mNetCommands << std::endl;

	/* Instantiate ELMBs by iterating over bus */

	for (::Bus::node_const_iterator it=bus->node().begin(); it!=bus->node().end(); it++)
	{
		const Node& node = *it;
		if (node.type().compare("elmb")==0) {
			SimulatedCan::CElmb *e = new SimulatedCan::CElmb (node);
			//e->setSerialByBusNode(0,i+config.firstElmb);
			e->setCanFrameSender(this); /* this implements CCanFrameSender interface */
			impl->elmbs.push_back (e);
			impl->mCanBus.associateElmb(e);
		}
		else
			cerr << "Node id=" << node.id() << " not instantiated, type unknown: " << node.type() << endl;

	}
	impl->mNetCommands->registerBus(&impl->mCanBus);


	impl->m_idCanScanThread =
			pthread_create(&impl->m_hCanScanThread,NULL,&CanScanControlThread,
					(void *)this);
	return (!impl->m_idCanScanThread);
}

void CSimulatedCanScan::sendErrorMessage(const struct can_frame *errFrame)
{
	std::cout << "sendErrorMessage(): unimplemented" << std::endl;

}

void CSimulatedCanScan::dumpFramesLog()
{
	if (mFramesLogFilename.length()==0)
		throw runtime_error("dumpFramesLog(): file name is empty!");

	std::ofstream f;
	f.open(mFramesLogFilename.c_str(), std::fstream::app | std::fstream::out);
	f << "--- DUMP --- time diff in miliseconds" << endl;
	impl->mLock.lock();
	if (impl->m_FramesLog.size() > 0)
	{
		try
		{
			list<CCanFrameLogEntry>::iterator it = impl->m_FramesLog.begin();
			timeval lastTimeval = it->getTime();
			for (; it!=impl->m_FramesLog.end(); it++)
			{
				timeval t = it->getTime();
				f << "[" << setw(8) << 1000.0*(SimulatedCan::CUtils::getTimeDiff(&lastTimeval,&t)) << "] ";
				f << it->toString();
				f << endl;
				lastTimeval = t;
			}
		}
		catch (std::exception &e)
		{
			impl->mLock.unlock();
			throw (e);
		}
	}
	f.close();
}
