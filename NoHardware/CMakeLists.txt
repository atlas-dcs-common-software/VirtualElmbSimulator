# This creates standaloen executable
link_directories (${VirtualElmbSimulator_BINARY_DIR}/CoreSimulator)
include_directories (${VirtualElmbSimulator_SOURCE_DIR}/CoreSimulator)
add_executable (NoHardware main.cpp  $<TARGET_OBJECTS:LogIt>)
target_link_libraries (NoHardware CoreSimulator -lpthread -lPocoNet -lPocoUtil -lPocoFoundation   -lxerces-c -lboost_thread-mt)
