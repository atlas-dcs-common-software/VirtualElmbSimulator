#include "SimulatedCanScan.h"
#include <unistd.h>
#include <iostream>


#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <net/if.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/error.h>
#include <libsocketcan.h>
#include <errno.h>

using namespace std;

void sendCanMessage (const canMessage& msg)
{
	/* just print it */
	cout << "sendmsg: id="<<hex<<msg.c_id<<" dlc="<<msg.c_dlc<<" data=";
	for (int i=0; i<msg.c_dlc; i++)
		cout << msg.c_data[i] << " ";
	cout << endl;
}



int main (int argc, const char* argv[])
{
	if (argc != 2)
	{
		cout << "./simulatedcan <name of can port>" << endl;
		return 1;
	}


	CSimulatedCanScan canscan;
	canscan.canMessageCame.connect (sendCanMessage);
	canscan.createBUS (argv[1], "params");
	cout << "OK probably will be running silently now; just press Ctrl-C " << endl;
	while (true)
	{
		sleep (1);
	}
}
